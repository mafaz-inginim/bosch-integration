﻿using System;
using MB9.Core.Web.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace MB9.Core.Web.Filter
{
   public class UserAuthorizeFilterAttribute :ActionFilterAttribute
    {
        public string role { get; set; }


        public override void OnActionExecuting(ActionExecutingContext filtercontext)
        {

            var s = role;
            var user = SessionHelper.GetLoggedInUser();
            if (user == null)
            {
                filtercontext.Result = new RedirectToRouteResult(
                new RouteValueDictionary { { "area", "AdminUser" }, { "controller", "Access" }, { "action", "Login" } });

            }
            if (user != null && user.UserRole != null)
            {
                if (!string.IsNullOrEmpty(role))
                {
                    if (!user.IsAdmin)
                    {
                        filtercontext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary { { "area", "" }, { "controller", "BaseSecure" }, { "action", "Error" } });
                    }
                }
            }
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }
    }
}
