﻿using MB9.Core.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Web.Services
{
    public interface IAppSession
    {
        
        bool ValidateAndSaveLoginInfo(String fullName, String email, User usersRepo);

        void LogOffClear();

        User GetLoggedInUser();

        void ClearSessionInfo(bool clearCompanyInfo);

        string GetCurrentUserHost();

    }
}
