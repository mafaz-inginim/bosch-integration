﻿using MB9.Core.Model.User;
using MB9.Core.Session;
using MB9.Core.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace MB9.Core.Web.Services
{
   public  class AppSession :IAppSession
    {
        

    
       public virtual bool ValidateAndSaveLoginInfo(String fullName, String email,User usersRepo)
       {
           HttpSessionState session = HttpContext.Current.Session;

           if (!String.IsNullOrWhiteSpace(email))
           {
               if (string.IsNullOrWhiteSpace(fullName)) fullName = email;



               if (usersRepo != null)
               {
                   // login fine;
                   session[MB9SessionKeys.SESSION_LOGGEDIN_USER] = usersRepo;
                   session[MB9SessionKeys.SESSION_FULLNAME] = usersRepo.DisplayName;
                   session[MB9SessionKeys.SESSION_EMAIL] = usersRepo.Email;

                  return true; // login ok
               }

           }

           return false; // login failed
       }


       public virtual void LogOffClear()
       {
           // clear every thing except company name and id
           ClearSessionInfo(false);
       }

       public virtual void ClearSessionInfo(bool clearCompanyInfo)
       {
           HttpSessionState session = HttpContext.Current.Session;
           { // remove user related info 
               session.Remove(MB9SessionKeys.SESSION_LOGGEDIN_USER);
               session.Remove(MB9SessionKeys.SESSION_FULLNAME);
               session.Remove(MB9SessionKeys.SESSION_EMAIL);
               session.Remove(MB9SessionKeys.SESSION_LOGGEDIN_ORG);
               session.Remove(MB9SessionKeys.SESSION_USER_ISADMIN);
               session.Remove(MB9SessionKeys.SESSION_AUTH_INFO);
               session[MB9SessionKeys.SESSION_USER_LOGGEDIN] = false;
           }

           if (clearCompanyInfo)
           {
               session.Remove(MB9SessionKeys.ORG);
               session.Remove(MB9SessionKeys.SESSION_ORG_NAME);
           }
       }


       public virtual string GetCurrentUserHost()
       {
           return HttpContext.Current.Request.Headers["Host"];
       }


       public User GetLoggedInUser()
       {
           HttpSessionState session = HttpContext.Current.Session;

           User user = session[MB9SessionKeys.SESSION_LOGGEDIN_USER] as User;

           return user;
       }

    }
}
