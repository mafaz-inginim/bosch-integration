﻿using MB9.Core.Web.Services;
using MB9.Core.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;
using MB9.Core.Session;

namespace MB9.Core.Web.Utils
{
   public static class SessionHelper
    {
       
       
       public static User GetLoggedInUser()
       {
           HttpSessionState session = HttpContext.Current.Session;

           User user = session[MB9SessionKeys.SESSION_LOGGEDIN_USER] as User;
           
           return user;
       }

    }
}
