﻿using MB9.Core.Web.Filter;
using MB9.Core.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.Core.Web.Controllers
{

    [UserAuthorizeFilterAttribute]
    public class BaseSecureController : BaseCustomerController, IActionFilter
    {

        protected override void OnActionExecuting(ActionExecutingContext filtercontext)
        {
            var user = SessionHelper.GetLoggedInUser();
            if (user != null) 
            {
                ViewBag.topbar_name = user.DisplayName;
                ViewBag.currentUserRole = user.UserRole.Name;
                ViewBag.IsAdmin = user.IsAdmin;
            }

        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }



        public ActionResult Error()
        {
            return View("Error");
        }

    }
}
