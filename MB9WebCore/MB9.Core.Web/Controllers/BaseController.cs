﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MB9.Core.Web.Controllers
{
    public class BaseController : Controller
    {
        protected void SetSuccessMessage(string message)
        {
            ViewBag.SuccessMessage = message;
        }

        protected void SetErrorMessage(string message)
        {
            ViewBag.ErrorMessage = message;
        }
    }
}
