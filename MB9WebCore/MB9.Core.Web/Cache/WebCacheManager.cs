﻿using MB9.Core.Service.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MB9.Core.Web.Cache
{
    public class WebCacheManager : ICacheManager
    {
        public virtual object GetCache(string key)
        {
            return HttpContext.Current.Cache[key];
        }

        public virtual void PutCache(string key, object value)
        {
            PutCache(key, value, TimeSpan.FromHours(1.0));
        }

        public virtual void PutCache(string key, object value, TimeSpan duration)
        {
            HttpContext.Current.Cache.Add(key, value, null, DateTime.Now.Add(duration), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
        }
    }
}
