﻿using MB9.Core.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Service
{
    public interface IUserService
    {
        IList<User> GetAllUsers(string serachkey = "", int start = 0, int length = 0, int sortIndex = 0, string sortDirection="");

        void Create(User user);

        void Delete(int id);

        User GetUserById(int id);

        void Update(User user);

        IList<UserRole> GetAllUserRoles();

        User GetUserByUserName(string name);

    }
}
