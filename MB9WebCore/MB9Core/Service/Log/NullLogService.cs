﻿using MB9.Core.Model.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Service.Log
{
    public class NullLogService : ILogService
    {
        public void LogAudit(Model.Log.AuditLog.AuditType type, string action, string message, string user)
        {
           
        }

        public void LogError(Exception e, string src, string url, string user)
        {
           
        }

        public void LogError(string message, string src, string url, string user)
        {
           
        }

        public IList<AuditLog> GetAuditLogs(int skip, int limit, string sortRowId, bool asc)
        {
            return new List<AuditLog>();
        }

        public IList<ErrorLog> GetErrorLogs(int skip, int limit, string sortRowId, bool asc)
        {
            return new List<ErrorLog>();
        }

        public int GetAuditLogCount()
        {
            return 0;
        }

        public int GetErrorLogCount()
        {
            return 0;
        }

        public void ClearErrorLog()
        {
            
        }

        public void ClearAuditLog()
        {
            
        }
    }
}
