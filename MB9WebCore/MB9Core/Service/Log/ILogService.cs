﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using MB9.Core.Util;
using MB9.Core.Model.Log;

namespace MB9.Core.Service.Log
{
    public interface ILogService
    {

        void LogAudit(AuditLog.AuditType type, String action, String message, String user);

        void LogError(Exception e, string src, String url, String user);

        void LogError(String message, string src, String url, String user);

        IList<AuditLog> GetAuditLogs(int skip, int limit, string sortRowId, bool asc);

        IList<ErrorLog> GetErrorLogs(int skip, int limit, string sortRowId, bool asc);

        int GetAuditLogCount();

        int GetErrorLogCount();

        void ClearErrorLog();

        void ClearAuditLog();
    }

}