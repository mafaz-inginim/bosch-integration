﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.ComponentModel;
using System.Threading;
using MB9.Core.Service.Config;

namespace MB9.Core.Service.Email
{
    public class MailerUtil
    {
        protected IAppConfig _appConfig;
        protected IMailer _mailer;

        public MailerUtil(IAppConfig appConfig, IMailer mailer)
        {
            this._appConfig = appConfig;
            this._mailer = mailer;
        }

        private void SendEmailAsync(object stateInfo)
        {
            object[] sendmailParams = stateInfo as object[];

            if (sendmailParams != null)
            {

                _mailer.Send(sendmailParams[0] as string, //subject
                    sendmailParams[1] as string,  //body
                    sendmailParams[2] as List<string>, //to
                    sendmailParams[3] as string,  //from
                    sendmailParams[4] as string   //fromName
                    );
            }
        }


        public void SendAsync(String msgSubject, String msgBody, String toEmail)
        {
            String fromEmail = _appConfig.GetConfigurationSettingValue("email_from_address");
            String fromName = _appConfig.GetConfigurationSettingValue("email_from_name");

            SendAsync(msgSubject, msgBody, toEmail, fromEmail, fromName);
        }

        public void SendAsync(String msgSubject, String msgBody, String toEmail, String fromEmail, String fromName)
        {
            if (String.IsNullOrWhiteSpace(fromEmail)
                   || String.IsNullOrWhiteSpace(toEmail))
            {
                return;
            }
            List<string> toEmailList = new List<string>(2);
            toEmailList.Add(toEmail);

            ThreadPool.QueueUserWorkItem(SendEmailAsync, new object[] { msgSubject, msgBody, toEmailList, fromEmail, fromName});
        }

        public void send(String msgSubject, String msgBody, List<String> toEmails)
        {
            if (toEmails.Count()==0)
            {
                return;
            }

            // Configs
            String fromEmail = _appConfig.GetConfigurationSettingValue("email_from_address");
            String fromName = _appConfig.GetConfigurationSettingValue("email_from_name");
            _mailer.Send(msgSubject, msgBody, toEmails, fromEmail, fromName);

        }

    }
}