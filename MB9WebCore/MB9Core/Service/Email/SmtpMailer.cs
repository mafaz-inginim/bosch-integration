﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.ComponentModel;
using MB9.Core.Service.Log;
using MB9.Core.Service.Config;

namespace MB9.Core.Service.Email
{
    public class SmtpMailer : IMailer
    {

        protected IAppConfig _appConfig;
        protected ILogService _log;

        public SmtpMailer(IAppConfig appConfig, ILogService log)
        {
            this._appConfig = appConfig;
            this._log = log;
        }

        private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            MailMessage message = e.UserState as MailMessage;

            if (e.Cancelled)
            {
                _log.LogError("Send Email has been Cancelled(smtp)", "SendEmail: Cancelled", "", "");
            }
            if (e.Error != null)
            {
                _log.LogError(e.Error, "SendEmail(smtp): Failed", "", "");
            }
            else
            {
                //Console.WriteLine("Message sent.");
            }

            if (message != null)
            {
                //cleanup
                message.Dispose();
            }
        }

        public void Send(string msgSubject, string msgBody, List<String> toEmails, string fromEmail, string fromName)
        {
            try
            {
                
                String smtpHost = _appConfig.GetConfigurationSettingValue("smtp_host");
                int smtpPort = int.Parse(_appConfig.GetConfigurationSettingValue("smtp_port"));
                Boolean useSSL = bool.Parse(_appConfig.GetConfigurationSettingValue("smtp_use_ssl"));
                String login = _appConfig.GetConfigurationSettingValue("smtp_auth");
                String pass = _appConfig.GetConfigurationSettingValue("smtp_pass");

                if (String.IsNullOrWhiteSpace(smtpHost)
                    || String.IsNullOrWhiteSpace(fromEmail)
                    || toEmails.Count()==0)
                {
                    return;
                }
               
                //SmtpClient Setup
                SmtpClient client = new SmtpClient(smtpHost, smtpPort);
                client.EnableSsl = useSSL;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(login, pass);


                //Message
                MailAddress from = new MailAddress(fromEmail, fromName, System.Text.Encoding.UTF8);
                
                MailMessage message = new MailMessage();
                message.From = from;

                toEmails.ForEach(x => message.To.Add(new MailAddress(x)));

                message.Body = msgBody;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = msgSubject;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                
                // Set the method that is called back when the send operation ends.
                client.SendCompleted += new
                   SendCompletedEventHandler(SendCompletedCallback);

                client.SendAsync(message, message); //usertoken is message, so that can cleanup

            }
            catch (Exception e)
            {
                _log.LogError(e, "Send Email (smtp)", null, null);
            }
        }
    }
}
