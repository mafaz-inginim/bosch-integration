﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Service.Email
{
    public interface IMailer
    {
        void Send(string msgSubject, string msgBody, List<String> toEmails, string fromEmail, string fromName);
    }
}
