﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using MB9.Core.Model.Customer;

namespace MB9.Core.Service.Config
{
    public class CustomerContextUtil
    {

        public static DefaultCustomerContext CONTEXT
        {
            get;
            set;
        }
        static CustomerContextUtil()
        {
            CONTEXT = new DefaultCustomerContext();
        }


        public static IAppCustomer GetCurrentCompanyInfo()
        {
            return CONTEXT.GetCurrentCompanyInfo();
        }

        public static IAppCustomer GetCurrentCompanyInfo(bool refresh)
        {
            return CONTEXT.GetCurrentCompanyInfo(refresh);
        }

        public static void SetCompany(string companyID)
        {
            CONTEXT.SetCompany(companyID);
        }


        public static bool RefreshValues(IAppCustomer company)
        {
            return CONTEXT.RefreshValues(company);
        }
    }
}