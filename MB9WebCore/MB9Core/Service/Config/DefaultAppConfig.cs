﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Configuration;

namespace MB9.Core.Service.Config
{
    public class DefaultAppConfig : IAppConfig
    {

        public virtual string GetConfigurationSettingValue(string configName)
        {
            String value = WebConfigurationManager.AppSettings[configName];

            //App Settings
            if (String.IsNullOrEmpty(value))
            {
                value = ConfigurationManager.AppSettings[configName];
            }

            return value;
        }

        public virtual string GetConnectionString(string connectionStringID)
        {
           
            return ConfigurationManager.ConnectionStrings[connectionStringID].ConnectionString;

        }

        public virtual int GetConfigurationSettingIntValue(string configName, int defaultVal)
        {
            String strVal = GetConfigurationSettingValue(configName);
            try
            {
                return int.Parse(strVal);
            }
            catch (Exception)
            {
            }

            return defaultVal;
        }


        public virtual string ApplicationName
        {
            get { return "MB9 App Template"; }
        }

        public virtual string ApplicationTagLine
        {
            get { return "Default tag line"; }
        }
    }
}