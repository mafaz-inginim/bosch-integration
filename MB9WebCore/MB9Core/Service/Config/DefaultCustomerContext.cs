﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using MB9.Core.Model.Customer;
using MB9.Core.Session;

namespace MB9.Core.Service.Config
{
    public class DefaultCustomerContext
    {
        public DefaultCustomerContext()
        {

        }


        public virtual IAppCustomer GetCurrentCompanyInfo()
        {
            return GetCurrentCompanyInfo(false);
        }

        public virtual IAppCustomer GetCurrentCompanyInfo(bool refresh)
        {

            return null;
        }

        public virtual void SetCompany(string companyID)
        {
            HttpContext.Current.Response.Cookies.Remove(MB9SessionKeys.ORG_COOKIE_NAME);
            HttpCookie companyCkie = new HttpCookie(MB9SessionKeys.ORG_COOKIE_NAME, companyID);
            companyCkie.Expires = DateTime.Now.AddDays(7);

            HttpContext.Current.Response.Cookies.Add(companyCkie);
            HttpContext.Current.Session[MB9SessionKeys.ORG] = companyID;
        }

        public virtual bool RefreshValues(IAppCustomer company)
        {
            return false;
        }
    }
}