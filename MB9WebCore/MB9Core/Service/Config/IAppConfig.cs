﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;

namespace MB9.Core.Service.Config
{
    public interface IAppConfig
    {

        String GetConfigurationSettingValue(string configName);
        
        String GetConnectionString(string connectionStringID);
        
        int GetConfigurationSettingIntValue(string configName, int defaultVal);
        
        String ApplicationName { get; }

        String ApplicationTagLine { get; }
    }
}