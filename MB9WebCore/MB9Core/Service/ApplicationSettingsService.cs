﻿using MB9.Core.Model.ApplicationSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Service
{
    public class ApplicationSettingsService : IApplicationSettingsService
    {
        private readonly IRepository<ApplicationSettings> _applicationSettingsRepository;

        public ApplicationSettingsService(IRepository<ApplicationSettings> applicationSettingsRepository)
        {
            this._applicationSettingsRepository = applicationSettingsRepository;
        }

        public void Create(Model.ApplicationSettings.ApplicationSettings appSetting)
        {
            if(appSetting == null)
            {
                throw new ArgumentNullException("App Settings");
            }
            _applicationSettingsRepository.Insert(appSetting);
        }

        public void Delete(int Id)
        {
            var appSettings = _applicationSettingsRepository.GetById(Id);
            _applicationSettingsRepository.Delete(appSettings);
        }

        public void Update(Model.ApplicationSettings.ApplicationSettings appSetting)
        {
            if(appSetting == null)
            {
                throw new ArgumentNullException("App Settings");
            }
            _applicationSettingsRepository.Update(appSetting);
        }

        public IList<Model.ApplicationSettings.ApplicationSettings> GetAllSettingsForClass(string classname)
        {
            IQueryable<ApplicationSettings> settings = 
                _applicationSettingsRepository.Table.Where(u => u.ClassName == classname);
            return settings.ToList();
        }
    }
}
