﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Service.Cache
{
    public interface ICacheManager
    {
        Object GetCache(string key);

        void PutCache(string key, Object value);

        void PutCache(string key, Object value, TimeSpan duration);
    }
}
