﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Service.Cache
{
    public class NoCacheManager : ICacheManager
    {
        public object GetCache(string key)
        {
            return null;
        }

        public void PutCache(string key, object value)
        {
            
        }

        public void PutCache(string key, object value, TimeSpan duration)
        {
           
        }
    }
}
