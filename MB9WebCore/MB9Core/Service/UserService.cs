﻿using MB9.Core.Entity;
using MB9.Core.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Service
{
    public class UserService : IUserService
    {
        private readonly UserRepository<User> _userRepository;

        private readonly UserRepository<UserRole> _userRoleRepository;


        public UserService(UserRepository<User> userRepository, UserRepository<UserRole> userRoleRepository)
        {

            this._userRepository = userRepository;
            this._userRoleRepository = userRoleRepository;
        }

        public IList<User> GetAllUsers(string searchkey="",int start=0,int length=0,int sortIndex=0,string sortDirection="")
        {

            var users = _userRepository.Table.Where(u => !u.Deleted);
            if(length!=0)
            {
                users = users.OrderBy(u=>u.Id).Skip(start).Take(length);
            }
            if(sortIndex==0)
            {
                if (sortDirection == "asc")
                    users = users.OrderBy(u => u.Id);
                else
                    users = users.OrderByDescending(u => u.Id);
            }

            if (sortIndex == 1)
            {
                if (sortDirection == "asc")
                    users = users.OrderBy(u => u.UserName);
                else
                    users = users.OrderByDescending(u => u.UserName);
            }

            if (sortIndex == 2)
            {
                if (sortDirection == "asc")
                    users = users.OrderBy(u => u.DisplayName);
                else
                    users = users.OrderByDescending(u => u.DisplayName);
            }

            if (sortIndex == 3)
            {
                if (sortDirection == "asc")
                    users = users.OrderBy(u => u.Email);
                else
                    users = users.OrderByDescending(u => u.Email);
            }

            if (!string.IsNullOrEmpty(searchkey))
                users = users.Where(u => u.UserName.Contains(searchkey) || u.Email.Contains(searchkey) || u.DisplayName.Contains(searchkey));
            return users.ToList();

        }

        public void Create(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            _userRepository.Insert(user);
        }

        public void Delete(int id)
        {

            var user = GetUserById(id);
            if (user == null)
                throw new Exception();
            user.Deleted = true;
            Update(user);

        }

        public User GetUserById(int id)
        {
            var user = _userRepository.GetById(id);
            return user;

        }

        public void Update(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            _userRepository.Update(user);
        }


        public IList<UserRole> GetAllUserRoles()
        {
            var userroles = _userRoleRepository.Table.Where(x=> x.Active==true).ToList();
            return userroles;
        }

        public User  GetUserByUserName(string username)
        {
            var user = _userRepository.Table.Where(u => (u.UserName == username||u.Email==username)&&!u.Deleted&&u.Active).FirstOrDefault();

            return user;

        }
    }
}
