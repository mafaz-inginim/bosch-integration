﻿using MB9.Core.Model;
using MB9.Core.Model.ApplicationSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Service
{
    interface IApplicationSettingsService
    {
        void Create(ApplicationSettings appSetting);

        void Delete(int Id);
        void Update(ApplicationSettings appSetting);

        IList<ApplicationSettings> GetAllSettingsForClass(string classname);

    }
}
