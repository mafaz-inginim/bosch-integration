﻿using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Web.Caching;
using System.Reflection;

namespace MB9.Core.Web
{
    public class AssemblyResourceProvider : VirtualPathProvider
    {
        public const string DEFAULT_PREFIX = "~/LIB_RES/";

        private string path_prefix;
       
        public AssemblyResourceProvider(String prefix)
        {
            path_prefix = prefix;
        }

        public AssemblyResourceProvider() : this(DEFAULT_PREFIX)
        {
        }

        private bool IsAppResourcePath(string virtualPath)
        {
            String checkPath = VirtualPathUtility.ToAppRelative(virtualPath);
            return checkPath.StartsWith(path_prefix, StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool FileExists(string virtualPath)
        {
            return (IsAppResourcePath(virtualPath) || base.FileExists(virtualPath));
        }


        public override VirtualFile GetFile(string virtualPath)
        {
            if (IsAppResourcePath(virtualPath))
                return new AssemblyResourceVirtualFile(virtualPath);
            else
                return base.GetFile(virtualPath);
        }

        public override CacheDependency GetCacheDependency(string virtualPath, IEnumerable virtualPathDependencies, DateTime utcStart)
        {
            if (IsAppResourcePath(virtualPath))
                return null;
            else
                return base.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart);
        }
    }

    public class AssemblyResourceVirtualFile : VirtualFile
    {
        string path;
        public AssemblyResourceVirtualFile(string virtualPath)
            : base(virtualPath)
        {
            path = VirtualPathUtility.ToAppRelative(virtualPath);
        }

        public override Stream Open()
        {
            string[] parts = path.Split('/');
            if (parts.Length < 4) return null; // [1]~/[2]LIB_RES/[3]DLL_NAME/[4]FileName
            string assemblyName = parts[2];
            string resourceName = parts[3];

            assemblyName = Path.Combine(HttpRuntime.BinDirectory, assemblyName);

            Assembly assembly = Assembly.LoadFile(assemblyName);
            if (assembly != null)
            {
                return assembly.GetManifestResourceStream(resourceName) ?? 
                    assembly.GetManifestResourceStream(resourceName + ".aspx") ??
                    assembly.GetManifestResourceStream(resourceName + ".ascx");
            }
            return null;
        }    

    }
}
