﻿using MB9.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Util.DataTable
{
    public static class DataTableHelper
    {



        public static SearchResult<T> SearchResponse<T>(T result, SearchHandler handler, int displayRecords, int totalRecords)
        {
            if (result == null)
            {
                return new SearchResult<T>
                {
                    error = "No Data Found"
                };
            }
            return new SearchResult<T>
            {
                draw = handler.draw,
                recordsFiltered = displayRecords,
                recordsTotal = totalRecords,
                data = result
            };

        }


        public static SearchHandler Process(System.Web.HttpRequestBase Request)
        {
            var searchHandler = new SearchHandler();

            searchHandler.draw = Request["draw"];
            searchHandler.start = Convert.ToInt32(Request["start"]);
            searchHandler.length = Convert.ToInt32(Request["length"]);
            searchHandler.GlobalSearch = Request["search[value]"];
            searchHandler.sortColumn = Convert.ToInt32(Request["order[0][column]"]);
            searchHandler.sortDir = Request["order[0][dir]"];

            return searchHandler;
        }
    }
}
