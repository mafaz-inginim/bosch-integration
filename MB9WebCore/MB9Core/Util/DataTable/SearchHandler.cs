﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Util.DataTable
{
    public class SearchHandler
    {

        /// <summary>
        /// Request sequence number sent by DataTable,
        /// same value must be returned in response
        /// </summary>       
        public string draw { get; set; }

        /// <summary>
        /// Text used for filtering
        /// </summary>
        public string GlobalSearch { get; set; }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        public int length { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        public int start { get; set; }

        /// <summary>
        /// Sorting Column
        /// </summary>
        public int sortColumn { get; set; }

        /// <summary>
        /// Sorting Directions
        /// </summary>
        public string sortDir { get; set; }
    }
}
