﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Util
{
    public class ErrorData
    {
        public ErrorData(String errorDetails)
            : this(null, errorDetails, null, -1) {
        }

        public ErrorData(String errorDetails, Exception ex)
            : this(null, errorDetails, ex, -1) {
        }

        public ErrorData(Exception ex)
            : this(null, ex != null ? ex.Message : "", ex, -1) {
        }

        public ErrorData(String errorDetails, int errorCode)
            : this(null, errorDetails, null, errorCode) {
        }

        public ErrorData(String source, String errorDetails)
            : this(source, errorDetails, null, -1)
        {

        }


        public ErrorData(String source, String errorDetails, Exception ex, int errorCode) {
            Details = errorDetails ?? "";
            Exception = ex;
            ErrorCode = errorCode;
            Source = source ?? "";
        }


        public String Details { get; private set; }
        public int ErrorCode { get; private set; }
        public Exception Exception { get; private set; }
        public String Source { get; private set; }

        public override string ToString() {
            StringBuilder err = new StringBuilder();
            err.AppendLine("[" + ErrorCode + "]" + Details);
            if (Exception != null) {
                err.AppendLine("Exception [" + Exception.GetType().ToString() + "]");
            }

            return err.ToString();
        }
    }
}
