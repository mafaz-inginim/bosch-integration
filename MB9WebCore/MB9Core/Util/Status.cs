﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Util
{
    public class Status : IStatus
    {

        List<ErrorData> _errors;

        public Status() {
            _errors = new List<ErrorData>();
        }


        public void AddError(String error) {
            _errors.Add(new ErrorData(error));
        }

        public void AddError(Exception ex) {
            _errors.Add(new ErrorData(ex));
        }

        public void AddError(String error, Exception ex) {
            _errors.Add(new ErrorData(error, ex));
        }

        public void AddError(String source, String error, Exception ex, int code) {
            _errors.Add(new ErrorData(null, error, ex, code));
        }

        public void AddError(String source, String error)
        {
            _errors.Add(new ErrorData(source, error));
        }

        public void AddError(String error, int code) {
            _errors.Add(new ErrorData(error, code));
        }

        public void AppendStatus(IStatus status) {
            foreach (ErrorData err in status.Errors) {
                _errors.Add(err);
            }
        }

        #region IStatus Members

        public bool IsOK {
            get { return (_errors == null || _errors.Count < 1); }
        }

        public IEnumerable<ErrorData> Errors {
            get { return _errors; }
        }

        public IEnumerable<String> ErrorsStrings {
            get { return _errors.Select(err => err.Details); }
        }

        public void Clear() {
            _errors.Clear();
        }

        #endregion

        public static Status OK_STATUS = new Status();

        public static Status CreateNewErrorStatus(String error) {
            Status st = new Status();
            st.AddError(error);
            return st;
        }

        public String StatusDump() {
            if (IsOK) {
                return "Status : OK";
            }

            StringBuilder str = new StringBuilder();
            str.AppendLine("Status : Error");

            foreach (ErrorData error in Errors) {
                str.AppendLine("Error : " + error.ToString());
            }

            return str.ToString();
        }

        public override string ToString() {
            return StatusDump();
        }
    }
}
