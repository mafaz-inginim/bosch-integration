﻿using System;
using System.Collections.Generic;

namespace MB9.Core.Util
{
    public interface IStatus
    {
        bool IsOK { get; }

        IEnumerable<ErrorData> Errors { get; }

    }
}
