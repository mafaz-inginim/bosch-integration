﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace MB9.Core.Model.ApplicationSettings.Entity
{
    public partial class ApplicationSettingsMap : EntityTypeConfiguration<ApplicationSettings>
    {
        public ApplicationSettingsMap()
        {
            this.HasKey(u => u.Id);
            this.Property(u => u.ClassName);
            this.Property(u => u.PropertyName);
            this.Property(u => u.Value);
        }
    }
}
