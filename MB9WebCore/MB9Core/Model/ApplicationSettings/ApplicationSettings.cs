﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Model.ApplicationSettings
{
    public class ApplicationSettings : BaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string ClassName { get; set; }
        public virtual string PropertyName { get; set; }
        public virtual string Value { get; set; }
    }
}
