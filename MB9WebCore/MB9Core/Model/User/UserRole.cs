﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Model.User
{
    public partial class UserRole : BaseEntity
    {
        private ICollection<Permission> _permissions;
        private ICollection<User> _users;

        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual bool Active { get; set; }


        public virtual ICollection<Permission> Permissions
        {
            get { return _permissions ?? (_permissions = new List<Permission>()); }
            protected set { _permissions = value; }
        }

        public virtual ICollection<User> Users
        {
            get { return _users ?? (_users = new List<User>()); }
            protected set { _users = value; }
        }

    }
}
