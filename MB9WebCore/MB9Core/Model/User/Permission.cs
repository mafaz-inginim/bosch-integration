﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Model.User
{
    public partial class Permission : BaseEntity
    {
        public ICollection<UserRole> _userRoles;

        public virtual int Id { get; set; }

        public virtual string Key { get; set; }

        public virtual string Name { get; set; }

        public virtual ICollection<UserRole> UserRoles
        {
            get { return _userRoles ?? (_userRoles = new List<UserRole>()); }
            protected set { _userRoles = value; }
        }

    }
}
