﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Model.User.Entity
{
    public partial class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            this.HasKey(u => u.Id);
            this.Property(u => u.UserName).HasMaxLength(255);
            this.Property(u => u.DisplayName).HasMaxLength(255);
            this.Property(u => u.Email).HasMaxLength(255);
            this.Property(u=> u.Active);
            this.Property(u => u.IsAdmin);
            this.Property(u => u.Deleted);
            this.Property(u => u.Password);
            this.Property(u => u.PasswordSalt);
            this.HasRequired(u => u.UserRole)
                .WithMany(ur => ur.Users)
                .HasForeignKey(u => u.UserRoleId);
            
           
         }
    }
}
