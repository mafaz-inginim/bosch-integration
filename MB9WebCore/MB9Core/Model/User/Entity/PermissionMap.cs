﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Model.User.Entity
{
   public partial  class PermissionMap :EntityTypeConfiguration<Permission>
    {
       public PermissionMap()
       {
           this.HasKey(p=>p.Id);
           this.Property(p => p.Key).HasMaxLength(255);
           this.Property(p => p.Name).HasMaxLength(255);
           this.HasMany(p => p.UserRoles)
           .WithMany();
          
       }
    }
}
