﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Model.User.Entity
{
    public partial class UserRoleMap : EntityTypeConfiguration<UserRole>
    {
        public UserRoleMap()
        {
            this.HasKey(ur => ur.Id);
            this.Property(ur => ur.Name).HasMaxLength(255);
            this.Property(ur => ur.Active);
          
        }

    }
}
