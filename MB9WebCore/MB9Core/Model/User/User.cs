﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Model.User
{
   public partial class User : BaseEntity
    {

       public virtual int Id { get; set; }

       public virtual int UserRoleId { get; set; }

       public virtual string DisplayName { get; set; }

       public virtual string UserName { get; set; }

       public virtual string Email { get; set; }

       public virtual string Password { get; set; }

       public virtual string PasswordSalt { get; set; }

       public virtual bool Active { get; set; }

        public virtual bool Deleted { get; set; }

       public virtual UserRole UserRole { get; set; }

        public virtual bool IsAdmin { get; set; }




    }
}
