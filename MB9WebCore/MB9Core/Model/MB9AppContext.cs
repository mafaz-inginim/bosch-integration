﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MB9.Core.Model;
using MB9.Core.Model.Customer;
using MB9.Core.Model.Storage;

namespace MB9.Core.Model
{
    public class MB9AppContext
    {
        /// <summary>
        /// 
        /// Customer : is MB9 Helpdesk Client, who brought HelpDesk App
        /// 
        /// UserCompany : company of the currently logged in user;
        /// 
        /// </summary>
        /// <param name="currentCustomer"></param>
        public MB9AppContext(IAppCustomer currentCustomer)
        {
            CurrentCustomer = currentCustomer;
        }

        virtual public IAppCustomer CurrentCustomer { get; set; }


       virtual public bool CanAddAttachment () {
           return true;
       }


       virtual public void AddedAttachment(Attachment attachment)
       {
           // by default does nothing
       }
    }
}
