﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MB9.Core.Model.Customer;

namespace MB9.Core.Model
{
    public class BaseFactory
    {
        public static ICustomer CreateCustomer(String ID, String Name)
        {
            return new MB9.Core.Model.Customer.Customer(ID, Name);
        }
    }
}
