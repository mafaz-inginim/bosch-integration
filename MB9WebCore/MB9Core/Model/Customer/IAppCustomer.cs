﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MB9.Core.Model;


namespace MB9.Core.Model.Customer
{
    /// <summary>
    /// Information of Customer Who has brought this application from MB9
    /// 
    /// </summary>
    public interface IAppCustomer : ICustomer
    {
        /************  Basic  **************/
        // ID : unique string id, from ICustomer, 
        // Name : Company Full Name : eg: MB9 Pte Ltd.

        // Short Company Name eg : MB9
        String CallName { get; }
 
        //eg mb9inc.com users domain
        String EmailDomain { get; }

        // //eg mb9inc.com 
        String Domain { get; }

        IList<ServiceProviderInfo> ServiceProviders { get; } //Who sold this version{ self, longscale, GoogleApps, etc .. }

        #region Contact Info
        /************  Contact Info  **************/
        String ContactName { get; }
        String ContactEmail { get; }
        String ContactPhone { get; }

        String Addr_Line1 { get; }
        String Addr_Line2 { get; }
        String Addr_City { get; }
        String Addr_State { get; }
        String Addr_Country { get; }
        String Addr_PIN { get; }
        
        #endregion


        #region Admin Info
        /************  Admin Details **************/

        // Email Address of Admin
        String AdminEmail { get; }
        String AdminName { get; }

        #endregion 


        #region Options and Settings
        /************  Options **************/

        // User need to be there in User list. admin has to add explicitely
        // when set, user should be there in user list.
        bool DomainUsersRequirePermission { get; }

        // allow only email address with the company email address domain
        // when set to false, any user can login (eg: john@gmail.com will work)
        // when set, only users with company domain id will allowed to login
        bool AllowOnlyCompanyDomain { get; }

        // if external domain email is allowed, then they need explit permission
        // when set, john@gmail.com should be there is user list.
        bool ExternalUserRequirePermission { get; }

        // WindowsLive = 1, OpenID, AD, localtable,  (right now only WindowsLive)
        int AuthMethod { get; }

        //Authentication method of customers of this Customer
        int CustomerAuthMethod { get; }

        // This company account is enabled or not;
        bool Enabled { get; }

        bool AllowExternalAdmin { get; }


        /*
         * TO Add
         *  - Auth Details for particular domain.
         *  - Database Connection String
         *  - Databse Name 
         *  - Mapping String storage (optional)
         */
        
        #endregion 

  

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullName">Full Name to use, if user data doesnot exists</param>
        /// <param name="emailID">emailID, unique id of the users</param>
        /// <returns>return the ITickerUser if user can log in else null</returns>
        //IUser DoLogin(String fullName, String emailID, UsersRepository repo); 

        ///// <summary>
        ///// return the ITickerUser;
        ///// </summary>
        ///// <param name="emailID"></param>
        ///// <param name="repo"></param>
        ///// <returns></returns>
        //IUser GetUser(String emailID, UsersRepository repo);

        /// <summary>
        /// Return true, if user can login with admin permission
        /// </summary>
        /// <param name="emailID"></param>
        /// <returns></returns>
        bool CanLoginWithPermission(String emailID);


        int MaxTableSizeBytes { get; }
        int CurrentTableSizeBytes { get; }
        int MaxAttachStoreSizeBytes { get; }
        int CurrentAttachStoreSizeBytes { get; }
        int MaxUsers { get; }
    }
}
