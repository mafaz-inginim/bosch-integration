﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Model.Customer
{
    public class ServiceProviderInfo
    {
        public ServiceProviderInfo()
        {
            ContactEmail = String.Empty;
            CreatorInfo = String.Empty;
        }
        public virtual int ID { get; set; }

        public virtual IAppCustomer Customer { get; set; }

        public virtual string ProviderCustomerID { get; set; }

        public virtual String ServiceProviderID { get; set; }

        public virtual String ContactEmail { get; set; }

        public virtual String CreatorInfo { get; set; }

        // if customer is allowed to use this provider for login etc;
        // the customer account may be active, but not via this provider
        public virtual bool Active { get; set; }
    }
}
