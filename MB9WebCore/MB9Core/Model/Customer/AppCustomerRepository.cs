﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Model.Customer
{
    public interface AppCustomerRepository
    {
        AppCustomerFactory GetFactory();

        IAppCustomer GetCustomerByDomain(String domain);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">unique string prefix id for this company</param>
        /// <returns></returns>
        IAppCustomer GetCustomerByPrefixID(String id);

        void AddOrUpdateCustomer(IAppCustomer customer);

        void DeleteCustomer(IAppCustomer customer);

        void Save();

        void CloseSession();
    }
}
