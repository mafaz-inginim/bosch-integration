﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MB9.Core.Model.Customer;

namespace MB9.Core.Model.Customer
{
    public class Customer : ICustomer
    {
        public Customer(String id, String name)
        {
            ID = id;
            Name = name;
        }
        #region IEntity Members

        public string Name
        {
            get;
            set;
        }

        public string ID
        {
            get;
            private set;
        }

        #endregion
    }
}
