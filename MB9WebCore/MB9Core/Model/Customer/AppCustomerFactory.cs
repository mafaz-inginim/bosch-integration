﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Model.Customer
{
    public interface AppCustomerFactory
    {
        IAppCustomer CreateNewAppCustomer();
        IAppCustomer CreateNewAppCustomer(String id, String name, string emailDomain);
    }
}
