﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace MB9.Core.Model.Storage
{
    public class FileBasedAttachmentHandler : IAttachmentHandler
    {
        private const String FileDirectoryPath = "~/App_Data/ATTACHDB/";

        // This one is simpler timestamp based unique filename. 
        // NB: duplicated in S3AttachmentHandler as well.
        protected virtual string GenerateUniqueFileName(string filename) {

            // get the extension
            string ext = Path.GetExtension(filename);
            string newFileName = "";

            string timestamp = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            newFileName = Path.GetFileNameWithoutExtension(filename) + "_" + timestamp + ext;

            return newFileName;
        }

        protected virtual string GenerateUniqueFileName(string filename, HttpServerUtilityBase server, MB9AppContext context)
        {

            // get the extension
            string ext = Path.GetExtension(filename);
            string newFileName = "";

            // generate filename, until it's a unique filename
            bool unique = false;

            do
            {
                Random r = new Random();
                newFileName = Path.GetFileNameWithoutExtension(filename) + "_" + r.Next().ToString() + ext;
                unique = !System.IO.File.Exists(getDirectoryPath(server, context) + newFileName);
            } while (!unique);
            return newFileName;
        }

        private static string getDirectoryPath(HttpServerUtilityBase server, MB9AppContext context)
        {
            string ContextFilePath = FileDirectoryPath + context.CurrentCustomer.ID + @"/";

            String fullPath = server.MapPath(ContextFilePath);

            if (!System.IO.Directory.Exists(fullPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(fullPath);
                }
                catch (Exception)
                {

                }
            }
            return fullPath; 
        }




        #region IAttachmentHandler Members

        public string GetAttachmentFullURL(System.Web.HttpServerUtilityBase server, Attachment attachment, MB9AppContext context)
        {
            string fullfilename = getDirectoryPath(server, context) + attachment.URL;
            return fullfilename;
        }

        public bool DeleteAttachmentFile(System.Web.HttpServerUtilityBase server, Attachment attachment, MB9AppContext context)
        {
            string fullfilename = GetAttachmentFullURL(server, attachment, context);

            return DeleteFile(fullfilename);
            if (File.Exists(fullfilename))
            {
                File.Delete(fullfilename);
                return true;
            }
            return false;
            
        }

        public string SaveAttachmentFile(System.Web.HttpServerUtilityBase server, HttpPostedFileBase postFile, MB9AppContext context)
        {

            string filename = GenerateUniqueFileName(Path.GetFileName(postFile.FileName), server, context);
            postFile.SaveAs(getDirectoryPath(server, context) + filename);

            return filename;
        }


        public Stream OpenRead(HttpServerUtilityBase server, Attachment attachment, MB9AppContext context)
        {
            String filePath = attachment != null ? GetAttachmentFullURL(server, attachment, context) : null;
            if (filePath != null && System.IO.File.Exists(filePath))
            {
                return new System.IO.FileStream(filePath, System.IO.FileMode.Open);
            }
            return null;
        }


        public string SaveFile(Stream inputStream, string toAttachmentFileName, bool randomizeName) {
            string filename = FileDirectoryPath + 
                ( randomizeName ? GenerateUniqueFileName(toAttachmentFileName) : toAttachmentFileName);
            using (FileStream sr = new FileStream(filename, FileMode.Truncate)) {
                inputStream.CopyTo(sr);
            }

            return filename;
        }

        public bool DeleteFile(string attachmentFileName) {
            string filename = FileDirectoryPath + attachmentFileName;
            if (File.Exists(filename)) {
                File.Delete(filename);
                return true;
            }

            return false;
        }

        #endregion
    }
}
