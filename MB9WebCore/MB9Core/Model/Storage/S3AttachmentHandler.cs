﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using System.Configuration;
using Amazon.S3;
using Amazon.S3.Model;

namespace MB9.Core.Model.Storage
{
    public class S3AttachmentHandler : IAttachmentHandler
    {
        private string AWSAccessKey;
        private string AWSSecretKey;
        private string Bucket;
        //private AmazonS3 S3Client;//to do

        private S3CannedACL DefaultFileACL = S3CannedACL.PublicRead; // TODO: make this configurable.

        public S3AttachmentHandler() {
            AWSAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
            AWSSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
            Bucket = ConfigurationManager.AppSettings["S3AttachmentBucket"];
            //S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(AWSAccessKey, AWSSecretKey);
          
            // Create the bucket if the bucket doesnt exist.
            //try {
            //    PutBucketRequest request = new PutBucketRequest();
            //    request.BucketName = Bucket;
            //    S3Client.PutBucket(request);
            //} catch (AmazonS3Exception amazonS3Exception) {
            //    throw amazonS3Exception;
            //}

        }


        protected virtual string GenerateUniqueFileName(string filename)
        {

            // get the extension
            string ext = Path.GetExtension(filename);
            string newFileName = "";

            string timestamp = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            newFileName = Path.GetFileNameWithoutExtension(filename) + "_" + timestamp + ext;

            return newFileName;
        }



        #region IAttachmentHandler Members

        public string GetAttachmentFullURL(System.Web.HttpServerUtilityBase server, Attachment attachment, MB9AppContext context)
        {
            // We always give store full URL while saving to S3
            return attachment.URL;
        }

        public bool DeleteAttachmentFile(System.Web.HttpServerUtilityBase server, Attachment attachment, MB9AppContext context)
        {
            string fullfilename = GetAttachmentFullURL(server, attachment, context);

            return DeleteFile(fullfilename);
        }

        public string SaveAttachmentFile(System.Web.HttpServerUtilityBase server, HttpPostedFileBase postFile, MB9AppContext context)
        {
            string filename = GenerateUniqueFileName(Path.GetFileName(postFile.FileName));

            return SaveFile(postFile.InputStream, filename, false);
        }


        public Stream OpenRead(HttpServerUtilityBase server, Attachment attachment, MB9AppContext context)
        {
            /*
            String filePath = attachment != null ? GetAttachmentFullURL(server, attachment, context) : null;
            if (filePath != null && System.IO.File.Exists(filePath))
            {
                return new System.IO.FileStream(filePath, System.IO.FileMode.Open);
            }
            return null;
             */
            // Not supported. Just use the s3 full URL.
            return null;
        }


        public string SaveFile(Stream inputStream, string toAttachmentFileName, bool randomizeName) {
            string fileName = (randomizeName ? GenerateUniqueFileName(toAttachmentFileName) : toAttachmentFileName);

            PutObjectRequest putReq = new PutObjectRequest();
            //putReq.WithInputStream(inputStream);
            //putReq.WithBucketName(Bucket)
            //    .WithKey(fileName)
            //    .WithCannedACL(DefaultFileACL);
            //PutObjectResponse putResp = S3Client.PutObject(putReq);

            string url = "http://" + Bucket + ".s3.amazonaws.com/" + fileName;

            return url;
        }


        public Boolean DeleteFile(string attachmentFileName) {
            // strip the URL portion.
            string[] fileparts = attachmentFileName.Substring("http://".Length).Split(new char[]{'/'}, 2);
            string domain = fileparts[0]; // bucket.s3.amazonaws.com
            string bucket = domain.Split('.').First();
            string keyname = fileparts[1];

            // NB: We are not verifying the bucket == Bucket. 
            DeleteObjectRequest delObj = new DeleteObjectRequest();
            //delObj.WithBucketName(bucket).WithKey(keyname);
            //try {
            //    DeleteObjectResponse delResp = S3Client.DeleteObject(delObj);
            //    return true;
            //} catch (AmazonS3Exception e) {
            //    throw e;
            //}

            return false;
        }

        #endregion
    }
}
