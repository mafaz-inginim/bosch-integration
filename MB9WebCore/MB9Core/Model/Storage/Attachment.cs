﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Model.Storage
{
    public class Attachment 
    {
        public virtual int ID { get; private set; }


        public virtual string URL { get; set; }

        public virtual int StatusCode { get; set; }

        public virtual string Filename { get; set; }

        public virtual int FileSize { get; set; }

        public virtual string ContentType { get; set; }

        #region Attachment Members

        public virtual AttachmentStatus Status
        {
            get
            {
                if (StatusCode < 0)
                {
                    return AttachmentStatus.DELETED;
                }

                return AttachmentStatus.VALID;
            }
            set
            {
                if (value == AttachmentStatus.DELETED)
                {
                    StatusCode = -1;
                }
                else if (value == AttachmentStatus.VALID)
                {
                    StatusCode = 0;
                }
            }
        }

        #endregion
    }
}
