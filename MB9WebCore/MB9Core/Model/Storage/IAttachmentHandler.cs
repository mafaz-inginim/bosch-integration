﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace MB9.Core.Model.Storage
{
    public interface IAttachmentHandler
    {
        /// <summary>
        /// Return the URL for this attached file;
        /// </summary>
        /// <param name="server"></param>
        /// <param name="attachment"></param>
        /// <returns></returns>
        String GetAttachmentFullURL(HttpServerUtilityBase server, Attachment attachment, MB9AppContext context);

        /// <summary>
        /// Delete an attachment, return true on success
        /// </summary>
        /// <param name="server"></param>
        /// <param name="attachment"></param>
        /// <returns></returns>
        Boolean DeleteAttachmentFile(HttpServerUtilityBase server, Attachment attachment, MB9AppContext context);

        /// <summary>
        /// Create and Save an Attachment Return its relative URL
        /// </summary>
        /// <param name="server"></param>
        /// <param name="postFile"></param>
        /// <param name="comment"></param>
        /// <param name="ticket"></param>
        /// <returns>URL</returns>
        String SaveAttachmentFile(HttpServerUtilityBase server, HttpPostedFileBase postFile, MB9AppContext context);

        /// <summary>
        /// Return the OpenFileStream
        /// </summary>
        /// <param name="server"></param>
        /// <param name="attachment"></param>
        /// <returns></returns>
        Stream OpenRead(HttpServerUtilityBase server, Attachment attachment, MB9AppContext context);

        /// <summary>
        /// Takes a stream and saves to the attachment fileName given. toAttachmentFileName is 
        /// not to be altered. 
        /// Use randomizeFilename if you want to prevent overwrite.
        /// </summary>
        /// <param name="inputStream"></param>
        /// <param name="toAttachmentFileName"></param>
        /// <param name="randomizeName"></param>
        /// <returns>FileName if success. null if failed.</returns>
        String SaveFile(Stream inputStream, string toAttachmentFileName, bool randomizeName);

        /// <summary>
        /// Delete an attachment by filename.
        /// </summary>
        /// <param name="attachmentFileName"></param>
        /// <returns></returns>
        Boolean DeleteFile(string attachmentFileName);
    }
}
