﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Model.Log
{
    public abstract class Log
    {
        public virtual int ID { get; private set; }

        private string _message;
        public virtual string Message
        {
            get { return _message; }
            set
            {
                if (value != null && value.Length > 255)
                {
                    _message = value.Substring(0, 255);
                }
                else
                {
                    _message = value;
                }

            }
        }


        public virtual String UserID { get; set; }

        public virtual DateTime Timestamp { get; set; }
    }
}
