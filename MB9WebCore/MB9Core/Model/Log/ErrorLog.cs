﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Model.Log
{
    public class ErrorLog : Log
    {
        public static int MAX_TRACE = 5000;

        private string _type;
        public virtual string Type
        {
            get { return _type; }
            set
            {
                if (value != null && value.Length > 255)
                {
                    _type = value.Substring(0, 255);
                }
                else
                {
                    _type = value;
                }

            }
        }

        private string _src;
        public virtual string Source
        {
            get { return _src; }
            set
            {
                if (value != null && value.Length > 255)
                {
                    _src = value.Substring(0, 255);
                }
                else
                {
                    _src = value;
                }
            }
        }

        private string _url;
        public virtual string Url
        {
            get { return _url; }
            set
            {
                if (value != null && value.Length > 255)
                {
                    _url = value.Substring(0, 255);
                }
                else
                {
                    _url = value;
                }

            }
        }

        private string _trace;
        public virtual string Trace
        {
            get { return _trace; }
            set
            {
                if (value != null && value.Length > MAX_TRACE)
                {
                    _trace = value.Substring(0, MAX_TRACE);
                }
                else
                {
                    _trace = value;
                }

            }
        }

    }
}
