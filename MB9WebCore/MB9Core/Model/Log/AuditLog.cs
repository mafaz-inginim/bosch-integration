﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Model.Log
{
    public class AuditLog : Log
    {
        public enum AuditType
        {
            UNKNOWN = 0,
            ADMIN = 1,
            DELETE = 2,
            LOG_CLEAR = 3,
            OTHERS = 1000
        }


        public AuditLog()
        {
            Type = AuditType.UNKNOWN;
        }



        public virtual int TypeID { get; set; }

        public virtual AuditType Type
        {
            get
            {
                try
                {
                    return (AuditType)TypeID;
                }
                catch
                {
                    //
                }

                return AuditType.UNKNOWN;
            }
            set
            {
                TypeID = (int) Type;
            }
        }

        private string _action;
        public virtual string Action
        {
            get { return _action; }
            set
            {
                if (value != null && value.Length > 255)
                {
                    _action = value.Substring(0, 255);
                }
                else
                {
                    _action = value;
                }

            }
        }

    }
}
