﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.Core.Model
{
    public interface IEntity
    {
        String Name { get; }

        String ID { get; }
    }
}
