﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.SessionState;
//using MB9.Core.Model.User;
//using MB9.Core.Model.Customer;
//using Microsoft.IdentityModel.Claims;
//using Microsoft.IdentityModel.Web;

//namespace MB9.Web.Session
//{
//    public class ClaimBasedSession : AbstractAppSession
//    {
//        public const String CLAIM_TYPE_NAME = @"http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
//        public const String CLAIM_TYPE_EMAIL = @"http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress";
//        public const String CLAIM_COMP_ID = @"http://cloudsts.longscale.com/claims/companyid";


//        public override bool InitAccessSession(IAppCustomer company, UsersRepository userRepo)
//        {
//            HttpSessionState session = HttpContext.Current.Session;
//            if (HttpContext.Current.User.Identity.IsAuthenticated)
//            {
//                IUser user = session[MB9SessionKeys.SESSION_LOGGEDIN_USER] as IUser;

//                if (user != null) return true;

//                return InitClaimUser(company, userRepo);

//            }

//            ClearSessionInfo(false);

//            return false;

//        }

//        private bool InitClaimUser(IAppCustomer company, UsersRepository userRepo)
//        {
//            ClearSessionInfo(false);
//            var claimUser = HttpContext.Current.User.Identity as IClaimsIdentity;

//            if (claimUser != null && company != null && userRepo != null)
//            {
//                String name = GetClaim(claimUser, CLAIM_TYPE_NAME);
//                String email = GetClaim(claimUser, CLAIM_TYPE_EMAIL);

//                return ValidateAndSaveLoginInfo(name, email, company, userRepo);
//            }

//            return false;
//        }

//        public static  String GetClaim(IClaimsIdentity identity, String type)
//        {

//            Claim claim = identity.Claims.FirstOrDefault(c => c.ClaimType.Equals(type));

//            return claim != null ? claim.Value : null;
//        }


//        public override IUser GetLoggedInUser(IAppCustomer company, UsersRepository userRepo)
//        {
//            HttpSessionState session = HttpContext.Current.Session;

//            IUser user = session[MB9SessionKeys.SESSION_LOGGEDIN_USER] as IUser;

//            if (user == null && company != null && userRepo != null)
//            {
//                InitClaimUser(company, userRepo);
//                user = session[MB9SessionKeys.SESSION_LOGGEDIN_USER] as IUser;
//            }

//            return user;
//        }

       

//        public override void LogOffClear()
//        {
//            base.LogOffClear();

//            var module = HttpContext.Current.ApplicationInstance.Modules["WSFederationAuthenticationModule"] as WSFederationAuthenticationModule;
//            if(module != null)  module.SignOut(true);

//        }


//    }
//}