﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Security;
//using System.Web.SessionState;
//using MB9.Core.Model.Customer;
//using MB9.Core.Model.User;
//using MB9.Web.Context;

//namespace MB9.Web.Session
//{
//    public class FormsAuthSession : AbstractAppSession
//    {
//        public override bool InitAccessSession(IAppCustomer company, UsersRepository userRepo)
//        {
//            HttpSessionState session = HttpContext.Current.Session;
//            if (HttpContext.Current.User.Identity.IsAuthenticated)
//            {
//                // Should Call Only if Logged In

//                String[] saveduserInfos = HttpContext.Current.User.Identity.Name.Split(const_splitter);

//                if (RestoreSessionInfo(session, saveduserInfos))
//                {
//                    return true;
//                }
//            }

//            ClearSessionInfo(false);

//            return false;

//        }

//        protected virtual bool RestoreSessionInfo(HttpSessionState session, String[] saveduserInfos)
//        {
//            // if user info is fine & company should match loggedin company
//            if (saveduserInfos.Length == 3 && saveduserInfos[2].Equals(session[MB9SessionKeys.ORG] as String))
//            {
//                session[MB9SessionKeys.SESSION_EMAIL] = saveduserInfos[0]; //Email
//                session[MB9SessionKeys.SESSION_FULLNAME] = saveduserInfos[1]; //FullName
//                session[MB9SessionKeys.SESSION_LOGGEDIN_ORG] = saveduserInfos[2];

//                session[MB9SessionKeys.SESSION_USER_LOGGEDIN] = true;

//                return true;
//            }
//            return false;
//        }

//        private bool InitiSessionCompanyInfo()
//        {
//            HttpSessionState session = HttpContext.Current.Session;
//            if (String.IsNullOrEmpty(session[MB9SessionKeys.ORG] as String)
//                || String.IsNullOrEmpty(session[MB9SessionKeys.SESSION_ORG_NAME] as String))
//            {
//                return InitiSessionCompanyInfo(CustomerContextUtil.GetCurrentCompanyInfo());
//            }

//            return true;

//        }


//        public override bool ValidateAndSaveLoginInfo(String fullName, String email, 
//            IAppCustomer compInfo, UsersRepository usersRepo)
//        {

//            if(base.ValidateAndSaveLoginInfo(fullName, email, compInfo, usersRepo)) {
//                FormsAuthentication.SetAuthCookie(HttpContext.Current.Session[MB9SessionKeys.SESSION_AUTH_INFO] as String, false);
//                return true;
//            }

//            return false;
//        }

//        public override void LogOffClear()
//        {
//            base.LogOffClear();
//            FormsAuthentication.SignOut();
//        }

//        public override IUser GetLoggedInUser(IAppCustomer company, UsersRepository userRepo)
//        {
//            HttpSessionState session = HttpContext.Current.Session;

//            IUser user = session[MB9SessionKeys.SESSION_LOGGEDIN_USER] as IUser;

//            if (user == null && company != null && userRepo != null)
//            {
//                // not cached yet;
//                String email = session[MB9SessionKeys.SESSION_EMAIL] as String;

//                if (!String.IsNullOrWhiteSpace(email))
//                {
//                    user = company.GetUser(email, userRepo);

//                    if (user != null)
//                    {
//                        user = user.Clone();
//                        session[MB9SessionKeys.SESSION_LOGGEDIN_USER] = user;
//                    }

//                }

//            }

//            return user;
//        }   
//    }
//}