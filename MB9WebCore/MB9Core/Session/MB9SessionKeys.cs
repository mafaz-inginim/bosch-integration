﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MB9.Core.Session
{
    public class MB9SessionKeys
    {

        public const string ORG = "org";
        public const string ORG_COOKIE_NAME = "mb9-app-customer-org-name";
        public const string LOGIN_RETURN_URL = "LoginReturnUrl";
        public const string ASP_RETURN_URL = "ReturnUrl"; // Asp Login framework Return key


        //Session

        public const string SESSION_FULLNAME = "fullname";
        public const string SESSION_EMAIL = "email";
        public const string SESSION_LOGGEDIN_ORG = "loggedin.org";
        public const string SESSION_LOGGEDIN_USER = "loggedin.user";
        public const string SESSION_LOGGEDIN_USER_ZONEID = "loggedin.user.zoneid";
        public const string SESSION_USER_ISADMIN = "user.admin";
        public const string SESSION_USER_LOGGEDIN = "user.loggedin";

        public const string SESSION_ORG_NAME = "org.fullname";
        public const string SESSION_AUTH_INFO = "auth.info";

        //public const string SESSION_USER_ROLES_LIST = "userroles.all";



    }
}