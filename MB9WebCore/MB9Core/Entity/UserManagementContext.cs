﻿using MB9.Core.Model.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.Core.Entity
{
    public class UserManagementContext : DbContext
    {
        public UserManagementContext()
            : this("DefaultConnection")
        {

        }

        public UserManagementContext(String nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        public DbSet<User> Users { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<Permission> Permissions { get; set; }

    }
}
