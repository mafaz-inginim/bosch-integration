﻿using MB9.Core.Model;
using MB9.Core.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace MB9.Core.Entity
{
    public class UserRepository<T> :  EfRepository<T>  where T : BaseEntity
    {

        private UserManagementContext _context = new UserManagementContext();

        protected override DbContext _dbcontext
        {
            get { return _context;  }
        }
    }
}
