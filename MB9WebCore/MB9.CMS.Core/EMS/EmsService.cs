﻿using MB9.CMS.Core.CMS.Entity;
using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.EMS.Entity;
using MB9.CMS.Core.EMS.Model;
using MB9.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.CMS.Core.Services
{
    /***
     * Event management module
     */
    public class EmsService : IEmsService
    {
        private readonly EmsRepository<Event> _eventRepo;
        private readonly EmsRepository<EventRegister> _regEventRepo;
        private readonly EmsRepository<EventAttendee> _eventAttendeeRepo;
        private readonly CmsRepository<DocPart> _docPartRepo;
        private readonly CmsRepository<DocPartContent> _docPartContentRepo;
        private readonly EmsRepository<Language> _langRepo;
        private readonly ICmsService _cmsService;

        public EmsService(EmsRepository<Event> eventRepo, EmsRepository<EventRegister> regEventRepo, EmsRepository<EventAttendee> eventAttendeeRepo, CmsRepository<DocPart> docPartRepo,
            CmsRepository<DocPartContent> docPartContentRepo, EmsRepository<Language> langRepo, ICmsService cmsService)
        {
            this._eventRepo = eventRepo;
            this._regEventRepo = regEventRepo;
            this._eventAttendeeRepo = eventAttendeeRepo;
            this._docPartRepo = docPartRepo;
            this._docPartContentRepo = docPartContentRepo;
            this._langRepo = langRepo;
            this._cmsService = cmsService;
        }
        
        
        //Events
        public Event GetEvent(string eventName)
        {
            eventName = eventName ?? "";
            return GetAllEvents().FirstOrDefault(x => x.EventType.Equals(eventName));
        }

        public Event GetEvent(int id)
        {
            return GetAllEvents().FirstOrDefault(x => x.ID.Equals(id));
        }

        public IEnumerable<Event> GetAllEvents()
        {
            var eventList = _eventRepo.Table.ToList();
            return eventList;
        }

        
        //EventRegister
        public void Create(EventRegister regMember)
        {
            if (regMember == null)
                throw new ArgumentNullException("regMember");

            _regEventRepo.Insert(regMember);
        }

        public void Delete(int id)
        {

            var regMember = GetRegisteredUser(id);
            if (regMember == null)
                throw new Exception();

            _regEventRepo.Delete(regMember);

        }

        public void Update(EventRegister regMember)
        {
            if (regMember == null)
                throw new ArgumentNullException("regMember");
            _regEventRepo.Update(regMember);
        }

        public EventRegister GetRegisteredUser(string emailID)
        {
            emailID = emailID ?? "";
            return GetAllRegUsers().FirstOrDefault(x => x.EmailID.Equals(emailID));
        }

        public EventRegister GetRegisteredUser(int id)
        {
            return GetAllRegUsers().FirstOrDefault(x => x.ID.Equals(id));
        }

        public IEnumerable<EventRegister> GetAllRegUsers(int eventID)
        {
            return _regEventRepo.Table.Where(x => x.EventID == eventID);
        }

        public IEnumerable<EventRegister> GetAllRegUsers()
        {
            var regUsersList = _regEventRepo.Table.ToList();
            return regUsersList;
        }

        public IList<EventRegister> GetAllRegisteredUsers(string searchkey = "", int start = 0, int length = 0, int sortIndex = 0, string sortDirection = "")
        {

            var regUsers = _regEventRepo.Table.Where(u => u.ID > 0);
            if (length != 0)
            {
                regUsers = regUsers.OrderBy(u => u.ID).Skip(start).Take(length);
            }
            if (sortIndex == 0)
            {
                if (sortDirection == "asc")
                    regUsers = regUsers.OrderBy(u => u.ID);
                else
                    regUsers = regUsers.OrderByDescending(u => u.ID);
            }

            if (sortIndex == 1)
            {
                if (sortDirection == "asc")
                    regUsers = regUsers.OrderBy(u => u.Name);
                else
                    regUsers = regUsers.OrderByDescending(u => u.Name);
            }


            if (!string.IsNullOrEmpty(searchkey))
                regUsers = regUsers.Where(u => u.Name.Contains(searchkey) || u.EmailID.Contains(searchkey));
            return regUsers.ToList();

        }

        public IList<EventRegister> GetAllRegisteredUsers(int eventID, string searchkey = "", int start = 0, int length = 0, int sortIndex = 0, string sortDirection = "")
        {

            var regUsers = _regEventRepo.Table.Where(x => x.EventID == eventID && x.ID > 0);

            if (length != 0)
            {
                regUsers = regUsers.OrderBy(u => u.ID).Skip(start).Take(length);
            }
            if (sortIndex == 0)
            {
                if (sortDirection == "asc")
                    regUsers = regUsers.OrderBy(u => u.ID);
                else
                    regUsers = regUsers.OrderByDescending(u => u.ID);
            }

            if (sortIndex == 1)
            {
                if (sortDirection == "asc")
                    regUsers = regUsers.OrderBy(u => u.Name);
                else
                    regUsers = regUsers.OrderByDescending(u => u.Name);
            }


            if (!string.IsNullOrEmpty(searchkey))
                regUsers = regUsers.Where(u => u.Name.Contains(searchkey) || u.EmailID.Contains(searchkey));
            return regUsers.ToList();

        }

        //EventAttendee
        public void Create(EventAttendee eveAttendee)
        {
            if (eveAttendee == null)
                throw new ArgumentNullException("eveAttendance");



            _eventAttendeeRepo.Insert(eveAttendee);
        }

        public IEnumerable<EventAttendee> GetAttendees(int eventID) 
        {
            var eventAttendees = _eventAttendeeRepo.Table.ToList();
            return eventAttendees;
        }

        //Setup DocPart
        public DocPart SetUpDocPart(DocPart formCollectionVals, string lang)
        {
            DocPart docPart = null;
            DocDefinition docDef = null;

            var docType = formCollectionVals.DocType;
            docDef = _cmsService.GetDocDef(docType);
            int docPartID = formCollectionVals.ID;

            if (docDef != null && lang != null)
            {
                if (docPartID > 0)
                {
                    docPart = _cmsService.GetDocPart(docPartID);
                }
                else
                {
                    docPart = new DocPart();
                    int parentID = formCollectionVals.ParentId;
                    docPart.ParentId = parentID;

                    docPart.ParentProperty = formCollectionVals.ParentProperty;

                    docPart.DocType = docDef.DocType;
                }

                if (docPart != null)
                {
                    bool published = formCollectionVals.Published;
                    docPart.Published = published;
                }

            }
            return docPart;
        }

        //Setup DocPartContent
        public DocPartContent SetUpDocPartContent(DocPart formCollectionVals, string lang)
        {
            DocDefinition docDef = null;
            DocPart docPart = null;
            DocPartContent docPartContent = null;
            int docPartID = formCollectionVals.ID;
            var docType = formCollectionVals.DocType;
            docDef = _cmsService.GetDocDef(docType);

            if (docDef != null && lang != null)
            {
                if (docPartID > 0)
                {
                    docPart = _cmsService.GetDocPart(docPartID);
                    docPartContent = _cmsService.GetDocPartContent(docPartID, lang);
                }

                docPartContent = docPartContent ?? new DocPartContent() { Language = lang, DocPartId = docPartID };

            }
            return docPartContent;
        }


        //SaveEvent
        public bool SaveEvent(Event events, DocPart docpart, DocPartContent docPartContent)
        {
            if (SaveDocPart(docpart, docPartContent))
            {
                if (events.ID < 1)
                {
                    // doc part id is not allowed to change;
                    events.DocPartId = docpart.ID;
                    events.EventType = docpart.DocType;
                    _eventRepo.Insert(events);
                }
                else
                {
                    _eventRepo.Update(events);
                }

                return true;
            }

            return false;
        }

        //SaveDocPart
        public bool SaveDocPart(DocPart docpart, DocPartContent docPartContent)
        {
            try
            {
                if (docpart != null)
                {
                    if (docpart.ID < 1)
                    {
                        _docPartRepo.Insert(docpart);
                    }
                    else
                    {
                        _docPartRepo.Update(docpart);
                    }

                    if (docPartContent.ID < 1)
                    {
                        docPartContent.DocPartId = docpart.ID;
                        _docPartContentRepo.Insert(docPartContent);
                    }
                    else
                    {
                        _docPartContentRepo.Update(docPartContent);
                    }

                    return true;
                }
            }
            catch (Exception)
            {

            }

            return false;
        }

        //SaveRegistration
        public bool SaveEventRegistration(EventRegister regEvent)
        {
            try
            {
                if (regEvent != null)
                {

                    if (regEvent.ID < 1)
                    {
                        _regEventRepo.Insert(regEvent);
                    }
                    else
                    {
                        _regEventRepo.Update(regEvent);
                    }


                    return true;
                }
            }
            catch (Exception)
            {

            }

            return false;

        }

    }
}
