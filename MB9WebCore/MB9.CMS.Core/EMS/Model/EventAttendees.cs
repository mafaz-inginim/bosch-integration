﻿using MB9.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.EMS.Model
{
    [Table("EventAttendee")]
    public class EventAttendee : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        public int EventID { get; set; }

        public int EventRegisterID { get; set; }

        public string Name { get; set; }

        public string EmailID { get; set; }
        
        public bool AttendedEvent { get; set; }

        public bool AlumniMember { get; set; }
    }
}
