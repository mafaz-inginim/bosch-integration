﻿using MB9.CMS.Core.CMS.Model;
using MB9.Core.Model;
using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.EMS.Model
{
    [Table("Event")]
    public class Event : BaseEntity, IAbstractPage
    {
        public int ID { get; set; }

        public string EventType { get; set; }

        public DateTime EventStartDate { get; set; }

        public DateTime EventEndDate { get; set; }

        public DateTime RegStartDate { get; set; }

        public DateTime RegEndDate { get; set; }

        public int MaxAttendees { get; set; }

        public string SeoName { get; set; }

        public string Title { get; set; }

        public int DocPartId { get; set; }

        public string PageCategory { get; set; }

        public string ViewTemplate { get; set; }

        public bool Active { get; set; }

        public bool AutoInclusion { get; set; } //Auto include registrants to alumni group


    }
}
