﻿using MB9.Core.Model;
using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.EMS.Model
{
    [Table("EventRegister")]
    public class EventRegister : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        public int EventID { get; set; }

        public string EmailID { get; set; }

        public string Name { get; set; }

        public string PhoneNum { get; set; }

        public string RegistrationStatus { get; set; }

        public string AdditionalContent { get; set; }
    }
}
