﻿using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.EMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.CMS.Core.Services
{
    public interface IEmsService
    {
        Event GetEvent(string eventName);

        Event GetEvent(int id);

        IEnumerable<Event> GetAllEvents();

        EventRegister GetRegisteredUser(string emailID);

        EventRegister GetRegisteredUser(int id);

        IEnumerable<EventRegister> GetAllRegUsers(int eventID);

        IEnumerable<EventRegister> GetAllRegUsers();

        IList<EventRegister> GetAllRegisteredUsers(string searchkey = "", int start = 0, int length = 0, int sortIndex = 0, string sortDirection = "");

        IList<EventRegister> GetAllRegisteredUsers(int eventID, string searchkey = "", int start = 0, int length = 0, int sortIndex = 0, string sortDirection = "");

        void Create(EventRegister regMember);

        void Delete(int id);

        void Update(EventRegister regMember);

        void Create(EventAttendee eveAttendee);

        IEnumerable<EventAttendee> GetAttendees(int eventID);

        DocPart SetUpDocPart(DocPart formCollectionVals, string lang);

        DocPartContent SetUpDocPartContent(DocPart formCollectionVals, string lang);

        bool SaveEvent(Event events, DocPart docpart, DocPartContent docPartContent);

        bool SaveDocPart(DocPart docpart, DocPartContent docPartContent);

        bool SaveEventRegistration(EventRegister regEvent);
    }
}
