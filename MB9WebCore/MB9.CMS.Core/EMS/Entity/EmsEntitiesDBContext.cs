﻿using MB9.CMS.Core.CMS.Entity;
using MB9.CMS.Core.EMS.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.EMS.Entity
{
    public class EmsEntitiesDBContext : DbContext
    {
        public EmsEntitiesDBContext()
            : this("DefaultConnection")
        {

        }

        public EmsEntitiesDBContext(String nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }
   
        public DbSet<Event> Events { get; set; }
        public DbSet<EventRegister> EventRegs { get; set; }
        public DbSet<EventAttendee> EventAttendees { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Event>().Map(m =>
        //    {
        //        m.MapInheritedProperties();
        //        m.ToTable("Event");
        //    });
        //    base.OnModelCreating(modelBuilder);
        //}
    }
}
