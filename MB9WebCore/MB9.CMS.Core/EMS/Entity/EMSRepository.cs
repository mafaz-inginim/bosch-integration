﻿using MB9.Core.Model;
using MB9.Core.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.EMS.Entity
{
    public class EmsRepository<T> :  EfRepository<T>  where T : BaseEntity
    {

        private EmsEntitiesDBContext _context = new EmsEntitiesDBContext();

        protected override DbContext _dbcontext
        {
            get { return _context;  }
        }
    }
}
