﻿using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.CMS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.Services
{
    public interface IMenuService
    {
         void Create(Menu menu);

         Menu GetNode(string id);

         void Update(Menu menu);

         void Delete(string id);

         MenuView GetMenuAsTree(string language,string purpose="");

         List<MenuItemName> GetAllRootNodes(string language);

         List<MenuItemName> GetAllChildOfANode(string id);

         void getChildren(MenuView parent,string purpose = "");

         //void changePosition(int id, float weight);

         float getPosition(int id);

         Menu GetLowestRankChild(int id);

         Menu GetPreviousSibling(string id);

         Menu GetNextSibling(string id);


    }
}
