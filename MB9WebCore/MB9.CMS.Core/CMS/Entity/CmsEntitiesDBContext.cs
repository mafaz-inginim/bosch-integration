﻿using MB9.CMS.Core.CMS.Model;
using MB9.Core.Model.User.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MB9.CMS.Core.CMS.Entity
{
    public class CmsEntitiesDBContext : DbContext
    {
        public CmsEntitiesDBContext()
            : this ("DefaultConnection")
        {

        }

        public CmsEntitiesDBContext(String nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        public DbSet<DocPart> DocParts { get; set; }

        public DbSet<DocDefinition> DocDefinitions { get; set; }

        public DbSet<DocPartContent> DocPartContents { get; set; }

        public DbSet<Language> Languages { get; set; }

        public DbSet<PageMap> Pages { get; set; }

        public DbSet<ViewTemplate> ViewTemplates { get; set; }

        public DbSet<Settings> Settings { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<Menu> Menus { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<PageMap>().Map(m =>
        //    {
        //        m.MapInheritedProperties();
        //        m.ToTable("PageMap");
        //    });
        //    base.OnModelCreating(modelBuilder);
        //}

       
    }
}