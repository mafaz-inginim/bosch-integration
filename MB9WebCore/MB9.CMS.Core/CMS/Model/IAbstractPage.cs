﻿using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.Model
{
    public interface IAbstractPage
    {

        string SeoName { get; set; }

        string Title { get; set; }

        int DocPartId { get; set; }

        string PageCategory { get; set; }

        string ViewTemplate { get; set; }

        bool Active { get; set; }

    }
}
