﻿using MB9.Core.Model;
using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.CMS.Core.CMS.Model
{
    [Table("Settings")]
    public class Settings : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}
