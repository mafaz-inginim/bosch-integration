﻿using MB9.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.Model
{
    public class UserProfile : BaseEntity
    {
        public int ID { get; set; }

        public string UserName { get; set; }

        public string UserRole { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Address { get; set; }
    }
}
