﻿using MB9.Core.Model;
using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.Model
{
    public abstract class AbstractPage : BaseEntity , IAbstractPage
    {

        public string SeoName { get; set; }

        public string Title { get; set; }

        public int DocPartId { get; set; }

        public string PageCategory { get; set; }

        public string ViewTemplate { get; set; }

        public bool Active { get; set; }

    }
}
