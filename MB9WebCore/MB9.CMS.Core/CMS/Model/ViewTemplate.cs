﻿using MB9.Core.Model;
using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.Model
{
    [Table("ViewTemplate")]
    public class ViewTemplate : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        public string DisplayName { get; set; }

        public string DocType { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public bool Active { get; set; }
    }
}
