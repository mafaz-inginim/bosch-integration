﻿using MB9.Core.Model;
using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.Model
{
    [Table("DocDefinition")]
    public class DocDefinition : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        public string DocType { get; set; }

        public string Name { get; set; }

        [Column(TypeName = "text")]
        public string Definition { get; set; }

        public Boolean Visibility { get; set; }
    }
}
