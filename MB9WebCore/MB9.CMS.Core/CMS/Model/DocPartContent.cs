﻿using MB9.Core.Model;
using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.Model
{
    [Table("DocPartContent")]
    public class DocPartContent : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        public int DocPartId { get; set; }

        [Column(TypeName = "ntext")]
        public string Content { get; set; }

        public string Language { get; set; }

    }
}
