﻿using MB9.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.Model
{
    [Table("Menu")]
    public class Menu : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public int ParentId { get; set; }

        public string Language { get; set; }

        public string Tooltip { get; set; }

        public bool IsActive { get; set; }

        public string Link { get; set; }

        public float Position { get; set; }

    }
}
