﻿using MB9.Core.Model;
using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.Model
{
    [Table("Language")]
    public class Language : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        public string languageCode { get; set; }

        public string languageDisplayName { get; set; }

        public string languageName { get; set; }
    }
}
