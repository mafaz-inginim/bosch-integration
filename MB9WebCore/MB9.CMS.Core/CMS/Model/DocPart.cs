﻿using MB9.Core.Model;
using MB9.CMS.Core.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MB9.CMS.Core.CMS.Model
{
    [Table("DocPart")]
    public class DocPart : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        public string DocType { get; set; }

        public int ParentId { get; set; }

        public string ParentProperty { get; set; }

        public bool Published { get; set; }
    }
}