﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.CMS.Entity;
using MB9.CMS.Core.CMS.ViewModel;

namespace MB9.CMS.Core.Services
{
    public class MenuService : IMenuService
    {
        private readonly CmsRepository<Menu> _menuRepo;

        public MenuService(CmsRepository<Menu> menuRepo)
        {
            this._menuRepo = menuRepo;
        }

        public void Create(Menu menu)
        {
            float last = 0;
            var lastelement = _menuRepo.Table.OrderByDescending(m => m.Position).First();
            if (!lastelement.Equals(null))
            {
                last = lastelement.Position;
            }

            if (menu != null)
            {
                menu.Position = last + 1;
                _menuRepo.Insert(menu);
            }
        }

        public Menu GetNode(string id)
        {
            var menu = _menuRepo.GetById(Convert.ToInt32(id));
            return menu;
        }

        public void Update(Menu menu)
        {
            if (menu != null)
            {
                _menuRepo.Update(menu);
            }
        }
        
        public void Delete(string id)
        {
            var node = GetNode(id);
            var children = GetAllChildOfANode(id);
            node.IsActive = false;
            Update(node);
        }

        public MenuView GetMenuAsTree(string language, string purpose="")
        {
            var parent = new MenuView();
            parent.ID = 0;
            List<Menu> roots = null;
            if (purpose.Equals(""))
            {
                roots = _menuRepo.Table.Where(m => m.Language.Equals(language) && m.ParentId == 0).OrderBy(m=>m.Position).ToList<Menu>();
            }
            else
            {
                roots = _menuRepo.Table.Where(m => m.IsActive == true && m.Language.Equals(language) && m.ParentId == 0).OrderBy(m => m.Position).ToList<Menu>();
            }
            
            if (roots.Count() > 0)
            {
                foreach (var root in roots)
                {
                    MenuView menu = new MenuView();
                    menu.ID = root.ID;
                    menu.Name = root.Name;
                    menu.Link = root.Link;
                    menu.Tooltip = root.Tooltip;
                    getChildren(menu,purpose);
                    parent.children.Add(menu);
                }
            }
            return parent;
    
        }

        public List<MenuItemName> GetAllRootNodes(string language)
        {
            var root_nodes = _menuRepo.Table.Where(m => m.Language.Equals(language) && m.ParentId == 0).OrderBy(m => m.Position).ToList();
            List<MenuItemName> return_list = new List<MenuItemName>();
            
            foreach (var itm in root_nodes)
            {
                MenuItemName item = new MenuItemName();
                item.id = itm.ID;
                item.name = itm.Name;
                if (GetAllChildOfANode(item.id + "").Count > 0)
                {
                    item.hasChildren = true;
                }
                else
                {
                    item.hasChildren = false;
                }
                return_list.Add(item);
            }

            return return_list;
        }

        public List<MenuItemName> GetAllChildOfANode(string id)
        {
            int parent_id = Convert.ToInt32(id);
            var root_nodes = _menuRepo.Table.Where(m => m.ParentId == parent_id).OrderBy(m => m.Position).ToList();
            List<MenuItemName> return_list = new List<MenuItemName>();
            
            foreach (var itm in root_nodes)
            {
                MenuItemName item = new MenuItemName();
                item.id = itm.ID;
                item.name = itm.Name;
                if (GetAllChildOfANode(item.id + "").Count > 0)
                {
                    item.hasChildren = true;
                }
                else
                {
                    item.hasChildren = false;
                }
                return_list.Add(item);
            }

            return return_list;
        }

        public void getChildren(MenuView parent, string purpose = "")
        {
            List<Menu> children = null;
            if (purpose.Equals(""))
            {
                children = _menuRepo.Table.Where(m => m.ParentId == parent.ID).OrderBy(m => m.Position).ToList<Menu>();
            }
            else
            {
                children = _menuRepo.Table.Where(m => m.ParentId == parent.ID && m.IsActive == true).OrderBy(m => m.Position).ToList<Menu>();
            }

            
            if (children.Count() > 0)
            {
                foreach (var child in children)
                {
                    MenuView menu = new MenuView();
                    menu.ID = child.ID;
                    menu.Name = child.Name;
                    menu.Link = child.Link;
                    menu.Tooltip = child.Tooltip;
                    getChildren(menu);
                    parent.children.Add(menu);
                    

                }
            }
        }


        //public void changePosition(int id, float weight)
        //{
        //    Menu menu = _menuRepo.GetById(id);
        //    menu.Position = weight;
        //    Update(menu);
        //}

        public float getPosition(int id)
        {
            Menu menu = _menuRepo.GetById(id);
            return menu.Position;
        }


        public Menu GetLowestRankChild(int id)
        {
            Menu lastChild = null; ;
            lastChild = _menuRepo.Table.Where(m => m.ParentId == id).OrderByDescending(m => m.Position).FirstOrDefault();
            
            return lastChild;
        }

        public Menu GetPreviousSibling(string id)
        {
            Menu menu = _menuRepo.GetById(Convert.ToInt32(id));
            var siblings = _menuRepo.Table.Where(m => m.ParentId == menu.ParentId).OrderBy(m => m.Position).ToList();
            var index = siblings.IndexOf(menu);
            index--;
            Menu previousSibling = null;
            if(index>0){
                previousSibling = siblings.ElementAt(index);
            }
            return previousSibling;

        }

        public Menu GetNextSibling(string id)
        {
            Menu menu = _menuRepo.GetById(Convert.ToInt32(id));
            var siblings = _menuRepo.Table.Where(m => m.ParentId == menu.ParentId).OrderByDescending(m => m.Position).ToList();
            var index = siblings.IndexOf(menu);
            Menu previousSibling = null;
            index--;
            if (index > 0)
            {
                previousSibling = siblings.ElementAt(index);
            }
            return previousSibling;
        }


    }
}
