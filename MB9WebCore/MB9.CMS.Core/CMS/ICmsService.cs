﻿using MB9.CMS.Core.CMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MB9.CMS.Core.Services
{
    public interface ICmsService
    {
        DocDefinition GetDocDef(string dtype);

        IEnumerable<DocDefinition> GetAllDocDefs();

        DocPart GetDocPart(int id);

        IEnumerable<DocPart> GetDocParts();

        DocPartContent GetDocPartContent(int id);

        DocPartContent GetDocPartContent(DocPart part, string lang);

        DocPartContent GetDocPartContent(int partid, string lang);

        IEnumerable<DocPartContent> GetDocPartContents();

        IEnumerable<Language> GetAllLangs();

        IEnumerable<ViewTemplate> GetAllViewTemplates();

        PageMap GetPage(int id);

        IEnumerable<PageMap> GetAllPages();

        DocPart SetUpDocPart(DocPart formCollectionVals, string lang);

        DocPartContent SetUpDocPartContent(DocPart formCollectionVals, string lang);

        bool SavePage(PageMap pages, DocPart docpart, DocPartContent docPartContent);

        bool SaveDocPart(DocPart docpart, DocPartContent docPartContent);

        Settings GetSetting(string name);

        IEnumerable<Settings> GetAllSettings();

    }
}
