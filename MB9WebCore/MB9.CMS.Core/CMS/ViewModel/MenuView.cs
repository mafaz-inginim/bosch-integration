﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.ViewModel
{
    public class MenuView
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Tooltip { get; set; }

        public string Link { get; set; }

        public List<MenuView> children { get; set; }

        public MenuView()
        {
            this.children = new List<MenuView>();
        }
    }
}
