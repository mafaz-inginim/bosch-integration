﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MB9.CMS.Core.CMS.ViewModel
{
    public class MenuItemName
    {
       public int id{get; set;}

       public string name{get; set;}

       public bool hasChildren { get; set; }
    }
}
