﻿using MB9.CMS.Core.CMS.Model;
using MB9.Core.Service;
using MB9.CMS.Core.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MB9.CMS.Core.CMS.Entity;

namespace MB9.CMS.Core.Services
{
    public class CmsService : ICmsService
    {
        private readonly CmsRepository<DocDefinition> _docDefRepo;
        private readonly CmsRepository<DocPart> _docPartRepo;
        private readonly CmsRepository<DocPartContent> _docPartContentRepo;
        private readonly CmsRepository<Language> _langRepo;
        private readonly CmsRepository<PageMap> _pageRepo;
        private readonly CmsRepository<ViewTemplate> _viewTemplateRepo;
        private readonly CmsRepository<Settings> _settingRepo;

        public CmsService(CmsRepository<DocDefinition> docDefRepo, CmsRepository<DocPart> docPartRepo,
            CmsRepository<DocPartContent> docPartContentRepo, CmsRepository<Language> langRepo, CmsRepository<PageMap> pageRepo,
            CmsRepository<ViewTemplate> viewTemplateRepo, CmsRepository<Settings> settingRepo)
        {

            this._docDefRepo = docDefRepo;
            this._docPartRepo = docPartRepo;
            this._docPartContentRepo = docPartContentRepo;
            this._langRepo = langRepo;
            this._pageRepo = pageRepo;
            this._viewTemplateRepo = viewTemplateRepo;
            this._settingRepo = settingRepo;
        }


        //DocDef
        public DocDefinition GetDocDef(string dtype)
        {
            dtype = dtype ?? "";
            return GetAllDocDefs().FirstOrDefault(x => x.DocType.Equals(dtype));
        }

        public IEnumerable<DocDefinition> GetAllDocDefs()
        {
            var docDefList = _docDefRepo.Table.ToList();
            return docDefList;
        }

        //DocPart
        public DocPart GetDocPart(int id)
        {
            return GetDocParts().FirstOrDefault(x => x.ID == id);
        }

        public IEnumerable<DocPart> GetDocParts()
        {
            var docPartList = _docPartRepo.Table.ToList();
            return docPartList;
        }

        //DocPartContent
        public DocPartContent GetDocPartContent(int id)
        {
            return GetDocPartContents().FirstOrDefault(x => x.ID == id);

        }

        public DocPartContent GetDocPartContent(DocPart part, string lang)
        {
            if (part != null)
                return GetDocPartContent(part.ID, lang);
            return null;
        }

        public DocPartContent GetDocPartContent(int partid, string lang)
        {
            return GetDocPartContents().FirstOrDefault(x => x.DocPartId == partid && x.Language.Equals(lang));
        }

        public IEnumerable<DocPartContent> GetDocPartContents()
        {
            var docPartContentList = _docPartContentRepo.Table.ToList();
            return docPartContentList;
        }

        //Languages
        public IEnumerable<Language> GetAllLangs()
        {
            var langList = _langRepo.Table.ToList();
            return langList;
        }

        //ViewTemplates
        public IEnumerable<ViewTemplate> GetAllViewTemplates()
        {
            var viewTemplateList = _viewTemplateRepo.Table.ToList();
            return viewTemplateList;
        }

        //Pages
        public PageMap GetPage(int id)
        {
            return GetAllPages().FirstOrDefault(x => x.ID == id);

        }
        public IEnumerable<PageMap> GetAllPages()
        {
            var pageList = _pageRepo.Table.ToList();
            return pageList;
        }


        //Setup DocPart
        public DocPart SetUpDocPart(DocPart formCollectionVals, string lang)
        {
            DocPart docPart = null;
            DocDefinition docDef = null;

            var docType = formCollectionVals.DocType;
            docDef = GetDocDef(docType);
            int docPartID = formCollectionVals.ID;

            if (docDef != null && lang != null)
            {
                if (docPartID > 0)
                {
                    docPart = GetDocPart(docPartID);
                }
                else
                {
                    docPart = new DocPart();
                    int parentID = formCollectionVals.ParentId;
                    docPart.ParentId = parentID;

                    docPart.ParentProperty = formCollectionVals.ParentProperty;

                    docPart.DocType = docDef.DocType;
                }

                if (docPart != null)
                {
                    bool published = formCollectionVals.Published;
                    docPart.Published = published;
                }

            }
            return docPart;
        }

        //Setup DocPartContent
        public DocPartContent SetUpDocPartContent(DocPart formCollectionVals, string lang) 
        {
            DocDefinition docDef = null;
            DocPart docPart = null;
            DocPartContent docPartContent = null;
            int docPartID = formCollectionVals.ID;
            var docType = formCollectionVals.DocType;
            docDef = GetDocDef(docType);

            if (docDef != null && lang != null)
            {
                if (docPartID > 0)
                {
                    docPart = GetDocPart(docPartID);
                    docPartContent = GetDocPartContent(docPartID, lang);
                }

                docPartContent = docPartContent ?? new DocPartContent() { Language = lang, DocPartId = docPartID };

            }
            return docPartContent;
        }

        //SavePage
        public bool SavePage(PageMap pages, DocPart docpart, DocPartContent docPartContent)
        {
            if (SaveDocPart(docpart, docPartContent))
            {
                if (pages.ID < 1)
                {
                    // doc part id is not allowed to change;
                    pages.DocPartId = docpart.ID;
                    pages.Active = true;
                    _pageRepo.Insert(pages);
                }
                else
                {
                    _pageRepo.Update(pages);
                }

                return true;
            }

            return false;
        }


        //SaveDocPart
        public bool SaveDocPart(DocPart docpart, DocPartContent docPartContent)
        {
            try
            {
                if (docpart != null)
                {
                    if (docpart.ID < 1)
                    {
                        _docPartRepo.Insert(docpart);
                    }
                    else
                    {
                        _docPartRepo.Update(docpart);
                    }

                    if (docPartContent.ID < 1)
                    {
                        docPartContent.DocPartId = docpart.ID;
                        _docPartContentRepo.Insert(docPartContent);
                    }
                    else
                    {
                        _docPartContentRepo.Update(docPartContent);
                    }

                    return true;
                }
            }
            catch (Exception)
            {

            }

            return false;
        }

        public Settings GetSetting(string name)
        {
            return GetAllSettings().FirstOrDefault(x => x.Name == name);
        }

        public IEnumerable<Settings> GetAllSettings()
        {
            var settingsList = _settingRepo.Table.ToList();
            return settingsList;
        }
    }
}
