﻿using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.EMS.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MB9.CMS.Web.Models
{
    public class TemlatePreviewViewModel
    {
        public TemlatePreviewViewModel(DocPart docpart, DocPartContent doccontent, PageMap Page = null, Event eve = null)
        {
            docPart = docpart;

            docContent = doccontent;

            if (Page != null) 
            {
                page = Page;
            }

            if (eve != null) 
            {
                events = eve;
            }

            Content = (docContent != null && docContent.Content != null) ?
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(docContent.Content)
                    : new Dictionary<string, string>();

            
        }

        public DocPart docPart { get; private set; }

        public DocPartContent docContent { get; private set; }

        public PageMap page { get; private set; }

        public Event events { get; private set; }

        public Dictionary<string, string> Content { get; private set;}
    }
}