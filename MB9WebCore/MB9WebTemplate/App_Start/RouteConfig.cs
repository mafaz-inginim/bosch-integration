﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MB9.CMS.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("App_Data/images/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default_",
            //    url: "{lang}/{url}",
            //    defaults: new { controller = "PageView", action = "Index", lang = UrlParameter.Optional}
            //);
            routes.MapRoute(
                name: "Default_",
                url: "Preview/{action}/{lang}/{url}",
                defaults: new { controller = "PageView", lang = UrlParameter.Optional }
            );


            routes.MapRoute(null, "Thumbnails/{tmb}", new { controller = "Files", action = "Thumbs", tmb = UrlParameter.Optional });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}