﻿



function mb9_cms_populateNestedList(listid, listbodyid, parentid, parentprop, language, urltemplate) {
    console.log("pid-" + parentid + " dtype-" + parentprop);
    $.ajax({
        url: "../ListItems",
        dataType: "json",
        data: { parentId: parentid, parentprop: parentprop , lang : language},
        success: function (response) {
            $(listbodyid).empty();

            for (var i = 0; i < response.length; i++) {
                response[i].Content = JSON.parse(response[i].Content);

                console.log(language);
                var itemUrl = urltemplate.replace('0012345678900', response[i].DocPartId).replace('EN', response[i].Language);
                console.log("iUrl: " + itemUrl);
                    
                var item = "";
                for (j in response[i].Content) {
                    item = response[i].Content[j];

                    if (language != response[i].Language) {
                        item = item + " -[" + response[i].Language + "]";
                        //language = response[i].Language;
                        itemUrl = urltemplate.replace('0012345678900', response[i].DocPartId).replace('EN', language);
                        console.log("iUrlrelaced: " + itemUrl);
                    }
                    // take only the first entry
                    break;
                }

                var html = '<tr> <td class="col-sm-1">' + response[i].DocPartId
                    + '</td> <td id="dataTableContent" class="col-sm-5">' + item
                    + '</td> <td class="col-sm-2"><a class="glyphicon glyphicon-edit" href="' + itemUrl + '"></a></td> </tr>';

                $(listid).append(html);
            }
        },
        error: function (response) {
            alert.log("error" + response);
        },
        type: "GET"
    });
    return false;
};

// ----------------- elFinder Customization -----------
function getDefaultElFinderOptions() {
    var myCommands = elFinder.prototype._options.commands;
    var disabled = ['extract', 'getfile', 'download', 'edit', 'paste', 'archive', 'help', 'select', 'resize', 'duplicate', 'mkfile', 'copy', 'cut', 'search', 'view', 'sort'];
    $.each(disabled, function (i, cmd) {
        (idx = $.inArray(cmd, myCommands)) !== -1 && myCommands.splice(idx, 1);
    });


    return {
        commands: myCommands,
        uiOptions: {
            toolbar: [
                ['back', 'forward'],
                ['reload'],
                ['home', 'up'],
                ['upload', 'mkdir'],
                ['info'],
                ['rm'],
                ['rename'],
            ]
        },
        handlers: {
            getfile: {
                onlyURL: false,
                multiple: false,
                folders: false,
                oncomplete: ''
            },
        }
    };

}
