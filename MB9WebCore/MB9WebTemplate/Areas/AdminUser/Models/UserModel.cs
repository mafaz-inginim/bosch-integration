﻿using MB9.Core.Model.User;
using MB9.CMS.Core.CMS.Model;
using MB9.Core.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace MB9.CMS.Web.Areas.AdminUser.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Displayname { get; set; }

        public bool Active { get; set; }
        
        public string Password { get; set; }

        //public bool Deleted { get; set; }

        public int DocPartId { get; set; }

        public int UserRoleId { get; set; }

        public string Role { get; set; }

        public bool IsAdmin { get; set; }

        public void setModel(User user)
        
        {
            this.Id = user.Id;
            this.Username = user.UserName;
            this.Email = user.Email;
            this.Active = user.Active;
            //this.Deleted = user.Deleted;
            this.Displayname = user.DisplayName;
            this.UserRoleId = user.UserRoleId;
            this.Role = user.UserRole.Name;
            this.IsAdmin = user.IsAdmin;

        }

        public User setData(UserModel usermodel, User user)
        {
            user.UserName = usermodel.Username;
            user.DisplayName = usermodel.Displayname;
            user.Active = usermodel.Active;
            //user.Deleted = usermodel.Deleted;
            user.Email = usermodel.Email;
            user.UserRoleId = usermodel.UserRoleId;
            user.IsAdmin = usermodel.IsAdmin;
            return user;

        }

        public IEnumerable<SelectListItem> UserRoles { get; set; }

        public DocPartContent docpartContent { get; set; }
    }
}