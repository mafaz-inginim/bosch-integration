﻿using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminUser
{
    public class AdminUserAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AdminUser";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AdminUser_default",
                "AdminUser/{controller}/{action}/{id}",
                new { controller="Access", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
