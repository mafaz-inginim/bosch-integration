﻿using MB9.Core.Util;
using MB9.Core.Web.Services;
using MB9.Core.Web.Utils;
using MB9.CMS.Web.Areas.AdminUser.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using MB9.Core.Service;


namespace MB9.CMS.Web.Areas.AdminUser.Controllers
{
    public class AccessController : Controller
    {

        private readonly IUserService _userService;

        private readonly IAppSession _session;

        public AccessController(IUserService userService, IAppSession session)
        {

            this._userService = userService;
            this._session = session;
        }
        //
        // GET: /AdminUser/Access/
        [HttpPost]
        public ActionResult Login(UserModel model)
        {

            var user = _userService.GetUserByUserName(model.Username);
            if (user != null)
            {
                if (PasswordUtil.SaltedHashVerify(model.Password, user.Password))
                {
                    // password fine, now do other validations
                    if (_session.ValidateAndSaveLoginInfo(user.UserName, user.Email, user))
                    {
                        return RedirectToAction("Index", "Document", new { area = "AdminCMS" });
                    }
                }
            }
            SetTitle();
            return View(model);
        }

        public ActionResult Login()
        {
            SetTitle();
            UserModel model = new UserModel();

            return View(model);
        }

        private void SetTitle()
        {
            string title = MB9ConfigurationKeys.PROJECTTITLE;
            string tagline = MB9ConfigurationKeys.PROJECTTAGLINE;
            ViewBag.projecttitle = title;
            ViewBag.tagline = tagline;
        }

        public ActionResult Logout()
        {
            _session.LogOffClear();
            return RedirectToAction("Login", "Access");
        }

    }
}
