﻿using MB9.Core.Util;
using MB9.Core.Util.DataTable;
using MB9.Core.Web.Services;
using MB9.CMS.Web.Areas.AdminUser.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MB9.Core.Web.Controllers;
using MB9.CMS.Web.Helper;
using System.Web.Configuration;
using Newtonsoft.Json;
using MB9.Core.Service;
using MB9.CMS.Core.Services;
using MB9.Core.Model.User;
using MB9.CMS.Core.CMS.Model;
using MB9.Core.Web.Filter;

namespace MB9.CMS.Web.Areas.AdminUser.Controllers
{
    [UserAuthorizeFilterAttribute(role="Admin")]
    public partial class UserController : BaseSecureController
    {
        private readonly IUserService _userService;
        private readonly IAppSession _appSession;
        private readonly ICmsService _cmsService;
        private readonly IEmsService _emsService;
        CrudFunctions crFuncs;


        public UserController(IUserService userService, IAppSession appSession, ICmsService cmsService, IEmsService emsService)
        {
            this._userService = userService;
            this._appSession = appSession;
            this._cmsService = cmsService;
            this._emsService = emsService;
            crFuncs = new CrudFunctions(_cmsService, _emsService);
        }
        

        public ActionResult List()
        {
            var user = _appSession.GetLoggedInUser();
            
            UserModel model = new UserModel();
            GetAvailableUserRoles(model);
            //SetViewBagData();
            return View(model);

        }

        public ActionResult MemberList(int userRoleId) 
        {
            var users = _userService.GetAllUsers().Where(x => x.UserRoleId.Equals(userRoleId));
            return View(users);
        }

        //private void SetViewBagData()
        //{
        //    var user = _appSession.GetLoggedInUser();
        //    if (user != null)
        //        ViewBag.topbar_name = user.DisplayName;
        //}


        private void GetAvailableUserRoles(UserModel model)
        {
            List<SelectListItem> AvailableUserRoles = new List<SelectListItem>();
            var roles = _userService.GetAllUserRoles();

            AvailableUserRoles.AddRange(roles.Select(s =>
            {
                SelectListItem item = new SelectListItem()
                {
                    Text = s.Name,
                    Value = s.Id.ToString()
                };
                return item;
            }).ToList());
            model.UserRoles = AvailableUserRoles;
            SetRolesViewBag(model);
        }

        private void SetRolesViewBag(UserModel model)
        {
            var user = _appSession.GetLoggedInUser();
            if (user != null)
                ViewBag.avail_roles = model.UserRoles;
        }

        [HttpPost]
        public ActionResult CreateUpdate(UserModel usermodel)
        {
            if (!string.IsNullOrEmpty(usermodel.Displayname) && !string.IsNullOrEmpty(usermodel.Email) && !string.IsNullOrEmpty(usermodel.Username)
                )
            {
                if (usermodel.Id == 0 && !string.IsNullOrEmpty(usermodel.Password))   //create  
                {
                    User user = new User();
                    if (usermodel.IsAdmin)
                    {
                        usermodel.UserRoleId = 4;
                        //usermodel.UserRoles = "Both";
                    }
                    user = usermodel.setData(usermodel, user);
                    
                    user.Password = PasswordUtil.SaltedHash(usermodel.Password);
                    _userService.Create(user);
                    return Json(new { success = "saved" }, JsonRequestBehavior.AllowGet);
                    //return Json("saved", JsonRequestBehavior.AllowGet);
                }
                else      //update
                {
                    var user = _userService.GetUserById(usermodel.Id);
                    if (user != null)
                    {
                        if (usermodel.IsAdmin)
                        {
                            usermodel.UserRoleId = 4;
                            //usermodel.UserRoles = "Both";
                        }
                        user = usermodel.setData(usermodel, user);
                        if (!string.IsNullOrEmpty(usermodel.Password))
                        {
                            user.Password = PasswordUtil.SaltedHash(usermodel.Password);
                        }
                        _userService.Update(user);
                        return Json(new { success = "updated" }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            return Json(new { success = "failed" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeleteUser(int id)
        {
            try
            {
                _userService.Delete(id);
            }
            catch (Exception)
            {
                return Json("false", JsonRequestBehavior.AllowGet);
            }
            return Json("true", JsonRequestBehavior.AllowGet);
        }

        private static void SetModel(List<UserModel> userlistmodel, IList<User> userlist)
        {
            foreach (var user in userlist)
            {
                UserModel usermodel = new UserModel();
                usermodel.setModel(user);
                userlistmodel.Add(usermodel);
            }
        }

        public ActionResult UserList()
        {

            var handler = DataTableHelper.Process(Request);
            int displayRecords;
            int totalRecords;

            List<User> userlist = new List<User>();
            List<UserModel> userlistmodel = new List<UserModel>();
            totalRecords = _userService.GetAllUsers().Count();

            userlist = _userService.GetAllUsers(handler.GlobalSearch, handler.start, handler.length, handler.sortColumn, handler.sortDir).ToList();
            SetModel(userlistmodel, userlist);
            if (!string.IsNullOrEmpty(handler.GlobalSearch))
            {
                displayRecords = userlist.Count();
                totalRecords = displayRecords;

            }
            else
            {
                displayRecords = totalRecords;
            }



            return Json(DataTableHelper.SearchResponse(userlistmodel, handler, displayRecords, totalRecords), JsonRequestBehavior.AllowGet);
        }


        public ActionResult DVRUserList()
        {

            var handler = DataTableHelper.Process(Request);
            int displayRecords;
            int totalRecords;

            List<User> userlist = new List<User>();
            List<UserModel> userlistmodel = new List<UserModel>();
            totalRecords = _userService.GetAllUsers().Count();

            userlist = _userService.GetAllUsers(handler.GlobalSearch, handler.start, handler.length, handler.sortColumn, handler.sortDir).Where(x=>x.UserRoleId==3).ToList();
            SetModel(userlistmodel, userlist);
            if (!string.IsNullOrEmpty(handler.GlobalSearch))
            {
                displayRecords = userlist.Count();
                totalRecords = displayRecords;

            }
            else
            {
                displayRecords = totalRecords;
            }



            return Json(DataTableHelper.SearchResponse(userlistmodel, handler, displayRecords, totalRecords), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DoorUserList()
        {

            var handler = DataTableHelper.Process(Request);
            int displayRecords;
            int totalRecords;

            List<User> userlist = new List<User>();
            List<UserModel> userlistmodel = new List<UserModel>();
            totalRecords = _userService.GetAllUsers().Count();

            userlist = _userService.GetAllUsers(handler.GlobalSearch, handler.start, handler.length, handler.sortColumn, handler.sortDir).Where(x => x.UserRoleId == 2).ToList();
            SetModel(userlistmodel, userlist);
            if (!string.IsNullOrEmpty(handler.GlobalSearch))
            {
                displayRecords = userlist.Count();
                totalRecords = displayRecords;

            }
            else
            {
                displayRecords = totalRecords;
            }



            return Json(DataTableHelper.SearchResponse(userlistmodel, handler, displayRecords, totalRecords), JsonRequestBehavior.AllowGet);
        }


        //DocPart
        public ActionResult CreateAddInfoDocPart(String doctype, String lang = null, int pid = -1, string pmemeber = null)
        {
            DocDefinition docDef;
            DocPart doc;
            DocPartContent docContent;

            crFuncs.createDocPartUtils(doctype, lang, pid, pmemeber, out docDef, out doc, out docContent);

            return ShowAddInfoDocPart(doc, docDef, docContent);
        }

        [HttpGet]
        public ActionResult EditAddInfoDocPart(int id, String lang = null)
        {
            DocDefinition docDef;
            DocPart doc;
            DocPartContent docPartContent;
            crFuncs.editDocPartUtils(id, lang, out doc, out docDef, out docPartContent);

            return ShowAddInfoDocPart(doc, docDef, docPartContent);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditAddInfoDocPart(FormCollection collection)
        {
            DocPart docPart = null;
            DocPartContent docPartContent = null;
            DocDefinition docDef = null;

            //crFuncs.LoadUpdateDocPart(collection, out docPart, out docPartContent, out docDef);

            var formCollectionVals = crFuncs.GetDocPartVals(collection);
            String lang = collection["lang"] ?? WebConfigurationManager.AppSettings["DefaultLanguage"];

            docPart = _cmsService.SetUpDocPart(formCollectionVals, lang);
            docPartContent = _cmsService.SetUpDocPartContent(formCollectionVals, lang);
            var docType = formCollectionVals.DocType;
            docDef = _cmsService.GetDocDef(docType);

            Dictionary<string, string> currentEntry = null;
            if (docPartContent.Content != null)
            {
                currentEntry = JsonConvert.DeserializeObject<Dictionary<string, string>>(docPartContent.Content);
            }

            docPartContent.Content = JsonConvert.SerializeObject(CrudFunctions.MergeContent(collection, currentEntry));
                

            int userId = int.Parse(collection["Id"]);
            var user = _userService.GetUserById(userId);
                
            if (docPart != null)
            {
                _cmsService.SaveDocPart(docPart, docPartContent);
                
                _userService.Update(user);
            }

            return Json(new { success = "updated" , response = docPart.ID }, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        private ActionResult ShowAddInfoDocPart(DocPart doc, DocDefinition docDef, DocPartContent docContent)
        {
            if (doc == null || docDef == null || docContent == null)
            {
                // return error;
            }

            var langs = _cmsService.GetAllLangs();
            var langList = CrudFunctions.LanguageDropdown(langs);

            var model = new DocPartViewModel(docDef, doc, docContent, langList);

            ViewBag.DocModel = model;

            return PartialView("AdditionalInfo", model);
            
        }
    }
}
