﻿using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.CMS.ViewModel;
using MB9.CMS.Core.Services;

using MB9.CMS.Web.Areas.AdminMenu.Models;
using MB9.Core.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminMenu.Controllers
{
    public class MenuController : BaseSecureController
    {
        private readonly ICmsService _cmsService;
        private readonly IMenuService _menuService;
        
        public MenuController(ICmsService cmsService, IMenuService menuService)
        {
            this._cmsService = cmsService;
            this._menuService = menuService;
        }
        //
        // GET: /AdminMenu/Menu/


        public ActionResult index(string language = "ENG")
        {


            var Languages = from n in _cmsService.GetAllLangs()
                            select new SelectListItem() { Text = n.languageDisplayName, Value = n.languageDisplayName };
            var index_finder = Languages.ToList<SelectListItem>();

            SelectListItem item = Languages.Where(m => m.Text.Equals(language)).FirstOrDefault();
            int index = index_finder.IndexOf(index_finder.Find(m => m.Text.Equals(item.Text)));
            var languages_temp = Languages.Where(m => !m.Text.Equals(item.Text)).ToList<SelectListItem>();
            item.Selected = true;

            languages_temp.Insert(index, item);
            Languages = languages_temp;

            return View(Languages);
        }


        public ActionResult GetChildren(string id)
        {
            var children = _menuService.GetAllChildOfANode(id);
            return Json(children, JsonRequestBehavior.AllowGet);
        }



        public ActionResult FormLoader(string id = "0", string p_id="0")
        {
            MenuFormModel model = new MenuFormModel(); 
            if (!id.Equals("0"))
            {
                var node=_menuService.GetNode(id);
                model.ID = node.ID;
                model.Language = node.Language;
                model.Link = node.Link;
                model.ParentId = node.ParentId;
                model.IsActive = node.IsActive;
                model.Name = node.Name;
                model.Tooltip = node.Tooltip;
                
            }

            else
            {
                model.ParentId=Convert.ToInt32(p_id);
            }

            model.Languages=from n in _cmsService.GetAllLangs()
                           select new SelectListItem() { Text = n.languageDisplayName, Value = n.languageDisplayName };
            return PartialView("_addMenuForm", model);
            //return View(model);
        }

        [HttpPost]
        public ActionResult CreateUpdate(MenuFormModel model)
        {
            
            Menu menu = new Menu();
            menu.Name = model.Name;
            menu.ParentId = model.ParentId;
            menu.Language = model.Language;
            menu.Link = model.Link;
            menu.Tooltip = model.Tooltip;
            menu.IsActive = model.IsActive;
            if (model.ID == 0)
            {
                
                try
                {
                    _menuService.Create(menu);
                    var menu_tree = _menuService.GetMenuAsTree(menu.Language, "display");

                    HttpRuntime.Cache.Insert(menu.Language + "_menu", menu_tree, null, DateTime.UtcNow.AddHours(1), Cache.NoSlidingExpiration);

                    return Json(new { success = "saved", tree = menu_tree }, JsonRequestBehavior.AllowGet);
                }
                catch(Exception e)
                {
                    
                    return Json(new { success = "error" }, JsonRequestBehavior.AllowGet);
                }
            }

            else
            {
                
                menu.ID = model.ID;
                 try
                {
                    _menuService.Update(menu);

                    var menu_tree = _menuService.GetMenuAsTree(menu.Language, "display");

                    HttpRuntime.Cache.Insert(menu.Language + "_menu", menu_tree, null, DateTime.UtcNow.AddHours(1), Cache.NoSlidingExpiration);
                    
                    return Json(new { success = "saved",tree=menu_tree }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { success = "error" }, JsonRequestBehavior.AllowGet);
                }
            }
            
        }

        public ActionResult TreeLoader(string language="ENG")
        {
             var menu_tree = _menuService.GetMenuAsTree(language);

             
            return PartialView("_menuTreeDisplay", menu_tree);
        }


        public ActionResult ChangePosition(string ref_id, string hitMode, string id)
        {

            var menu = _menuService.GetNode(id);

            var ref_node = _menuService.GetNode(ref_id);

            if (hitMode.Equals("over"))
            {
                menu.ParentId = ref_node.ID;
                var lastRankChild = _menuService.GetLowestRankChild(ref_node.ID);
                if(lastRankChild!=null)
                menu.Position = lastRankChild.Position + 1;
                
            }


            else if (hitMode.Equals("before"))
            {
                menu.ParentId = ref_node.ParentId;
                var higher = _menuService.GetPreviousSibling(ref_id);
                float higher_position = ref_node.Position-1;
                if (higher != null)
                {
                    higher_position = higher.Position;
                }
                menu.Position = (_menuService.getPosition(ref_node.ID) + higher_position) / 2;
            }

            else if (hitMode.Equals("after"))
            {
                menu.ParentId = ref_node.ParentId;
                var lower = _menuService.GetNextSibling(ref_id);
                float lower_position = ref_node.Position + 1;
                if (lower != null)
                {
                    lower_position = lower.Position;
                }
                menu.Position = (_menuService.getPosition(ref_node.ID) + lower_position) / 2;
            }

            _menuService.Update(menu);
            var menu_tree = _menuService.GetMenuAsTree(menu.Language, "display");
            HttpRuntime.Cache.Insert(menu.Language + "_menu", menu_tree, null, DateTime.UtcNow.AddHours(1), Cache.NoSlidingExpiration);
            return Json(new { success = "success" }, JsonRequestBehavior.AllowGet);
            
        }
       

    }
}
