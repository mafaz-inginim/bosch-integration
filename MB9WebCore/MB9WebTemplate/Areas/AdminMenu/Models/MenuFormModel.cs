﻿using MB9.CMS.Core.CMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminMenu.Models
{
    public class MenuFormModel:Menu
    {
        public IEnumerable<SelectListItem> Languages { get; set; }
    }
}