﻿using MB9.CMS.Core.CMS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminMenu.Models
{
    public class MenuTreeLanguageListModel
    {
        public MenuView MenuTree { get; set; }

        public IEnumerable<SelectListItem> Languages { get; set; }
    }
}