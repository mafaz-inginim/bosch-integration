﻿using MB9.CMS.Web.Areas.AdminEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using Newtonsoft.Json;
using MB9.CMS.Web.Helper;
using MB9.Core.Web.Services;
using MB9.Core.Util.DataTable;
using MB9.Core.Web.Controllers;
using MB9.Core.Web.Filter;
using MB9.Core.Web.Utils;
using MB9.CMS.Core.Services;
using MB9.CMS.Core.EMS.Model;
using MB9.CMS.Core.CMS.Model;

namespace MB9.CMS.Web.Areas.AdminEMS.Controllers
{
    //[UserAuthorizeFilterAttribute(role="Admin")]
    public class EventController : BaseSecureController
    {
        //
        // GET: /AdminEMS/Event/
        
        private readonly ICmsService _cmsService;
        private readonly IEmsService _emsService;
        CrudFunctions crFuncs;

        public EventController(ICmsService cmsService, IEmsService emsService)
        {
            this._cmsService = cmsService;
            this._emsService = emsService;
            crFuncs = new CrudFunctions(_cmsService , _emsService);
        }

        public ActionResult Index()
        {
            var events = _emsService.GetAllEvents();
            var langs = _cmsService.GetAllLangs();
            //var regMembers = _emsService.GetAllRegUsers();
            
            var upComingEvents = events.Where(x => x.EventStartDate > DateTime.Today);
            var ongoingEvents = events.Where(x => x.EventStartDate < DateTime.Today && x.EventEndDate > DateTime.Today);
            var pastEvents = events.Where(x => x.EventStartDate < DateTime.Today && x.EventEndDate < DateTime.Today);

            EventIndexViewModel model = new EventIndexViewModel();
            
            model.allEvents = events;
            model.allCurrentEvents = ongoingEvents;
            model.allUpComingEvents = upComingEvents;
            model.allPastEvents = pastEvents;

            model.allLangs = CrudFunctions.LanguageDropdown(langs);

            //model.MemberCount = regMembers.Count();
            
            return View(model);
        }

        public ActionResult Calendar()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ListEvents()
        {
            var events = _emsService.GetAllEvents();
            return Json(events, JsonRequestBehavior.AllowGet);
        }

        //Event
        public ActionResult Create(String pageCateg = null, String lang = null)
        {
            var doctype = WebConfigurationManager.AppSettings["DefaultEventTemplate"];

            DocDefinition docDef;
            DocPart docpart;
            DocPartContent docContent;
            Event events;
            PageMap pageDetails;

            crFuncs.createUtils(doctype, pageCateg, lang, out docDef, out docpart, out docContent,out pageDetails, out events);

            return ShowEventPage(events, docDef, docpart, docContent);

        }

        [HttpGet]
        public ActionResult EditEvent(int id, String lang = null)
        {
            //PageMap page;

            Event pageEvents;
            DocPart docpart;
            DocDefinition docDef;
            DocPartContent docPartContent;

            pageEvents = _emsService.GetEvent(id);

            crFuncs.editGetUtils(id, lang, pageEvents, out docpart, out docDef, out docPartContent);

            return ShowEventPage(pageEvents, docDef, docpart, docPartContent);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditEvent(FormCollection collection)
        {
            int eventID = -1;
            int.TryParse(collection["eventID"], out eventID);
            var events = new Event();
            DocPart docPart = null;
            DocPartContent docPartContent = null;
            DocDefinition docDef = null;

            if (eventID > 0)
            {
                events = _emsService.GetEvent(eventID);

            }
            else
            {
                events = new Event();
            }

            if (events != null)
            {
                events.EventType = collection["EventType"];
                events.SeoName = collection["SeoName"];
                events.Title = collection["Title"] ?? "defaultTitle";
                events.EventStartDate = DateTime.Parse(collection["StartDate"]);
                events.EventEndDate = DateTime.Parse(collection["EndDate"]);
                events.RegStartDate = DateTime.Parse(collection["RegStartDate"]);
                events.RegEndDate = DateTime.Parse(collection["RegEndDate"]);
                events.MaxAttendees = int.Parse(collection["MaxAttendees"]);

                if (collection["EventActiveCheckbox"].Contains("true"))
                {
                    events.Active = true;
                }
                else
                {
                    events.Active = false;
                }

                if (collection["AlumniAutoInclusionCheckbox"].Contains("true"))
                {
                    events.AutoInclusion = true;
                }
                else
                {
                    events.AutoInclusion = false;
                }
                

                //crFuncs.LoadUpdateDocPart(collection, out docPart, out docPartContent, out docDef);

                var formCollectionVals = crFuncs.GetDocPartVals(collection);
                String lang = collection["lang"] ?? WebConfigurationManager.AppSettings["DefaultLanguage"];

                docPart = _cmsService.SetUpDocPart(formCollectionVals, lang);
                docPartContent = _cmsService.SetUpDocPartContent(formCollectionVals, lang);
                var docType = formCollectionVals.DocType;
                docDef = _cmsService.GetDocDef(docType);

                Dictionary<string, string> currentEntry = null;
                if (docPartContent.Content != null)
                {
                    currentEntry = JsonConvert.DeserializeObject<Dictionary<string, string>>(docPartContent.Content);
                }

                docPartContent.Content = JsonConvert.SerializeObject(CrudFunctions.MergeContent(collection, currentEntry));
                

                _emsService.SaveEvent(events, docPart, docPartContent);
            }

            return ShowEventPage(events, docDef, docPart, docPartContent);

        }

        [NonAction]
        private ActionResult ShowEventPage(Event events, DocDefinition docDef, DocPart doc, DocPartContent docContent)
        {
            if (events == null || docDef == null || doc == null || docContent == null)
            {
                // return error;
            }

            var langs = _cmsService.GetAllLangs();
            var langList = CrudFunctions.LanguageDropdown(langs);

            var model = new EventPageViewModel(events, docDef, doc, docContent, langList);

            ViewBag.DocModel = model;

            return View("EditEvent", model);
        }

        //DocPart
        public ActionResult CreateEventDocPart(String doctype, String lang = null, int pid = -1, string pmemeber = null)
        {
            DocDefinition docDef;
            DocPart doc;
            DocPartContent docContent;
            crFuncs.createDocPartUtils(doctype, lang, pid, pmemeber, out docDef, out doc, out docContent);

            return ShowEventDocPart(doc, docDef, docContent);

        }

        [HttpGet]
        public ActionResult EditDocPart(int id, String lang = null)
        {
            DocPart doc;
            DocDefinition docDef;
            DocPartContent docPartContent;
            crFuncs.editDocPartUtils(id, lang, out doc, out docDef, out docPartContent);

            return ShowEventDocPart(doc, docDef, docPartContent);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditDocPart(FormCollection collection)
        {
            DocPart docPart = null;
            DocPartContent docPartContent = null;
            DocDefinition docDef = null;
            //crFuncs.LoadUpdateDocPart(collection, out docPart, out docPartContent, out docDef);

            var formCollectionVals = crFuncs.GetDocPartVals(collection);
            String lang = collection["lang"] ?? WebConfigurationManager.AppSettings["DefaultLanguage"];

            docPart = _cmsService.SetUpDocPart(formCollectionVals, lang);
            docPartContent = _cmsService.SetUpDocPartContent(formCollectionVals, lang);
            var docType = formCollectionVals.DocType;
            docDef = _cmsService.GetDocDef(docType);

            Dictionary<string, string> currentEntry = null;
            if (docPartContent.Content != null)
            {
                currentEntry = JsonConvert.DeserializeObject<Dictionary<string, string>>(docPartContent.Content);
            }

            docPartContent.Content = JsonConvert.SerializeObject(CrudFunctions.MergeContent(collection, currentEntry));
                

            if (docPart != null)
            {
                _emsService.SaveDocPart(docPart, docPartContent);
            }

            return ShowEventDocPart(docPart, docDef, docPartContent);
        }

        [NonAction]
        private ActionResult ShowEventDocPart(DocPart doc, DocDefinition docDef, DocPartContent docContent)
        {
            if (doc == null || docDef == null || docContent == null)
            {
                // return error;
            }

            var langs = _cmsService.GetAllLangs();
            var langList = CrudFunctions.LanguageDropdown(langs);

            var model = new DocPartViewModel(docDef, doc, docContent, langList);

            ViewBag.DocModel = model;

            return View("EditDocPart", model);
        }

        //Event Registration
        public ActionResult RegisterList(int eventID, string eventTitle)
       {
            var regUsers = _emsService.GetAllRegUsers(eventID);
            
            var regStatusSettings = _cmsService.GetSetting("event.registration.status");
            List<string> regStatusOptions = regStatusSettings.Value.Split(',').ToList();

            var regUserModel = new EventRegisterViewModel();

            regUserModel.currentEvent = _emsService.GetEvent(eventID);
            regUserModel.registeredUsers = regUsers;
            regUserModel.RegistrationStatusList = from m in regStatusOptions
                                                  select new SelectListItem() { Text = m, Value = m };

            regUserModel.EventID = eventID;
            ViewBag.EventID = eventID;
            ViewBag.Eventtitle = eventTitle;

            return View(regUserModel);
        }

        private static void SetModel(List<EventRegisterViewModel> reglistmodel, IList<EventRegister> reglist)
        {
            foreach (var user in reglist)
            {
                EventRegisterViewModel regmodel = new EventRegisterViewModel();
                regmodel.setModel(user);
                reglistmodel.Add(regmodel);
            }
        }

        public ActionResult RegisterListData(int eventID)
        {
            var handler = DataTableHelper.Process(Request);
            int displayRecords;
            int totalRecords;

            List<EventRegister> reglist = new List<EventRegister>();
            List<EventRegisterViewModel> reglistmodel = new List<EventRegisterViewModel>();
            totalRecords = _emsService.GetAllRegisteredUsers().Count();

            reglist = _emsService.GetAllRegisteredUsers(eventID, handler.GlobalSearch, handler.start, handler.length, handler.sortColumn, handler.sortDir).ToList();
            
            SetModel(reglistmodel, reglist);

            if (!string.IsNullOrEmpty(handler.GlobalSearch))
            {
                displayRecords = reglist.Count();
                totalRecords = displayRecords;

            }
            else
            {
                displayRecords = totalRecords;
            }


            return Json(DataTableHelper.SearchResponse(reglistmodel, handler, displayRecords, totalRecords), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateUpdate(EventRegisterViewModel regUsermodel)
        {
            //int limit = regUsermodel.currentEvent.MaxAttendees;
            //int currentOccupancy = _emsService.GetAllRegUsers(regUsermodel.EventID).Count();

            if (!string.IsNullOrEmpty(regUsermodel.Name) && !string.IsNullOrEmpty(regUsermodel.EmailID) /*&& currentOccupancy < limit */ )
            {
                if (regUsermodel.ID < 1)   //create  
                {
                    EventRegister regUser = new EventRegister();
                    regUser = regUsermodel.setData(regUsermodel, regUser);
                    _emsService.Create(regUser);
                    
                    return Json(new { success = "saved" }, JsonRequestBehavior.AllowGet);
                    //return Json("saved", JsonRequestBehavior.AllowGet);
                }
                else      //update
                {
                    var regUser = _emsService.GetRegisteredUser(regUsermodel.ID);
                    if (regUser != null)
                    {
                        regUser = regUsermodel.setData(regUsermodel, regUser);
                        _emsService.Update(regUser);
                        return Json(new { success = "updated" }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            return Json(new { success = "failed" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeleteUser(int id)
        {
            try
            {
                _emsService.Delete(id);
            }
            catch (Exception)
            {
                return Json("false", JsonRequestBehavior.AllowGet);
            }
            return Json("true", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult RegistrantList(int eventID) 
        {
            var attendedUsers = _emsService.GetAttendees(eventID).ToList();

            var attUserIds = new HashSet<int>(attendedUsers.Select(au => au.EventRegisterID));

            var regUsers = _emsService.GetAllRegUsers(eventID);

            IEnumerable<EventRegister> notAttendedUsers = regUsers.Where(x => !attUserIds.Contains(x.ID)).ToList();
            
            EventAttendanceViewModel eventAttendanceModel = new EventAttendanceViewModel();
            eventAttendanceModel.EventID = eventID;
            eventAttendanceModel.ListOfNotAttendedRegistrants = notAttendedUsers;
            eventAttendanceModel.ListOfAttendees = attendedUsers;

            return View(eventAttendanceModel);
        }

        public ActionResult UpdateAttendeesList(int[] arrayOfIDs, int eventID)
        {
            var currentEvent = _emsService.GetEvent(eventID);

            for (int i = 0; i < arrayOfIDs.Length; i++) 
            {
                int curId = arrayOfIDs[i];
                var regUser = _emsService.GetRegisteredUser(curId);

                EventAttendee model = new EventAttendee();
                model.EventRegisterID = regUser.ID;
                model.EventID = regUser.EventID;
                model.Name = regUser.Name;
                model.EmailID = regUser.EmailID;
                model.AttendedEvent = true; 

                if (currentEvent.AutoInclusion)
                {
                    model.AlumniMember = true;
                }
                else 
                {
                    model.AlumniMember = false;
                }

                _emsService.Create(model);
            }

            return Json(new { success = "saved" }, JsonRequestBehavior.AllowGet);
        }


    }
}
