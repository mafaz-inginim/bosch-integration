﻿using MB9.CMS.Core.EMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminEMS.Models
{
    public class EventIndexViewModel
    {
        public IEnumerable<Event> allEvents { get; set; }

        public IEnumerable<Event> allUpComingEvents { get; set; }

        public IEnumerable<Event> allPastEvents { get; set; }

        public IEnumerable<Event> allCurrentEvents { get; set; }

        public IEnumerable<SelectListItem> allLangs { get; set; }

        public int MemberCount { get; set; }

    }
}