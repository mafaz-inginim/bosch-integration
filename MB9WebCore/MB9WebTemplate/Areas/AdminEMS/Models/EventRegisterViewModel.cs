﻿using MB9.CMS.Core.EMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminEMS.Models
{
    public class EventRegisterViewModel
    {
        public int ID { get; set; }

        public int EventID { get; set; }

        public string EmailID { get; set; }

        public string Name { get; set; }

        public string PhoneNum { get; set; }

        public string RegistrationStatus { get; set; }

        public IEnumerable<SelectListItem> RegistrationStatusList { get; set; }

        public string AdditionalContent { get; set; }

        public void setModel(EventRegister regMember)
        {
            this.ID = regMember.ID;
            this.EventID = regMember.EventID;
            this.Name = regMember.Name;
            this.EmailID = regMember.EmailID;
            this.PhoneNum = regMember.PhoneNum;
            this.RegistrationStatus = regMember.RegistrationStatus;
            this.AdditionalContent = regMember.AdditionalContent;

        }

        public EventRegister setData(EventRegisterViewModel regUsermodel, EventRegister regUser)
        {
            regUser.EventID = regUsermodel.EventID;
            regUser.Name = regUsermodel.Name;
            regUser.EmailID = regUsermodel.EmailID;
            regUser.PhoneNum = regUsermodel.PhoneNum;
            regUser.RegistrationStatus = regUsermodel.RegistrationStatus;
            regUser.AdditionalContent = regUsermodel.AdditionalContent;
            return regUser;

        }

        public Event currentEvent { get; set; }

        public EventRegister registerUser { get; set; }

        public IEnumerable<EventRegister> registeredUsers { get; set; }
    }
}