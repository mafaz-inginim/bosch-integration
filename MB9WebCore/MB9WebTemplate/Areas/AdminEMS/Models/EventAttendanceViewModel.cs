﻿using MB9.CMS.Core.EMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminEMS.Models
{
    public class EventAttendanceViewModel
    {
        public int EventID { get; set; }

        public IEnumerable<EventRegister> ListOfNotAttendedRegistrants { get; set; }

        public IEnumerable<EventAttendee> ListOfAttendees { get; set; }

    }
}