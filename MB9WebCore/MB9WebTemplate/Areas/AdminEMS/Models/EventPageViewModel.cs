﻿using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.EMS.Model;
using MB9.CMS.Web.Areas.AdminCMS.Models;
using MB9.CMS.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminEMS.Models
{
    public class EventPageViewModel : DocPartViewModel
    {
        public Event events { get; set; }

        public EventPageViewModel(Event eve, DocDefinition def, DocPart part, DocPartContent content, IEnumerable<SelectListItem> Langlist)
            : base(def, part, content, Langlist)
        {
            events = eve;
        }
    }
}