﻿using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminEMS
{
    public class AdminEMSAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AdminEMS";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AdminEMS_default",
                "AdminEMS/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
