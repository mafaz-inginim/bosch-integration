﻿using MB9.CMS.Core.CMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminCMS.Models
{
    public class DocIndexViewModel
    {
        public IEnumerable<SelectListItem> allDocs { get; set; }

        public IEnumerable<PageMap> allPages { get; set; }

        public IEnumerable<SelectListItem> allLangs { get; set; }
    }

}