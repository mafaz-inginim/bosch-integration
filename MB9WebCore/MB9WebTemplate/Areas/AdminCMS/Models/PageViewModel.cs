﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MB9.CMS.Web.Helper;
using MB9.CMS.Core.CMS.Model;

namespace MB9.CMS.Web.Areas.AdminCMS.Models
{
    public class PageViewModel : DocPartViewModel
    {

        public PageMap Page { get; set; }

        public PageViewModel(PageMap page, DocDefinition def, DocPart part, DocPartContent content, IEnumerable<SelectListItem> Langlist)
            : base(def, part, content, Langlist)
        {
            Page = page;
        }
    }
}