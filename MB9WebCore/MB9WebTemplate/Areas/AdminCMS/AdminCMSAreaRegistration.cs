﻿using System.Web.Mvc;

namespace MB9.CMS.Web.Areas.AdminCMS
{
    public class AdminCMSAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AdminCMS";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AdminCMS_default",
                "AdminCMS/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional } //area = "AdminCMS",
            );
        }
    }
}
