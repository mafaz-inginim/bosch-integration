﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MB9.CMS.Web.Areas.AdminCMS.Models;
using Newtonsoft.Json;
using MB9.CMS.Web.Helper;
using System.Web.Configuration;
using MB9.Core.Web.Services;
using MB9.Core.Web.Controllers;
using MB9.Core.Web.Filter;
using MB9.CMS.Core.Services;
using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.EMS.Model;
using Microsoft.Practices.Unity;
using MB9.Core.Service;

namespace MB9.CMS.Web.Areas.AdminCMS.Controllers
{
    //[UserAuthorizeFilterAttribute(role = "AdminPage")]
    public class DocumentController : BaseSecureController
    {
        private readonly ICmsService _cmsService;
        private readonly IEmsService _emsService;
        CrudFunctions crFuncs;

        public DocumentController(ICmsService cmsService, IEmsService emsService)
        {
            this._cmsService = cmsService;
            this._emsService = emsService;
            crFuncs = new CrudFunctions(_cmsService , _emsService);
        }

        public ActionResult Index() 
        {
            var documentDef = _cmsService.GetAllDocDefs().Where(x => x.Visibility.Equals(true));
            var pages = _cmsService.GetAllPages().Where(m => m.PageCategory != null);
            var langs = _cmsService.GetAllLangs();

            DocIndexViewModel model = new DocIndexViewModel();
            
            model.allLangs = CrudFunctions.LanguageDropdown(langs);

            model.allDocs = from m in documentDef
                            select new SelectListItem() { Text = m.Name, Value = m.DocType };

            model.allPages = pages;
            
            return View(model);
        }

        public ActionResult Blog()
        {
            var documentDef = _cmsService.GetAllDocDefs().Where(x => x.Visibility.Equals(true));
            var pages = _cmsService.GetAllPages().Where(m => m.PageCategory.Equals("blog"));
            var langs = _cmsService.GetAllLangs();

            DocIndexViewModel model = new DocIndexViewModel();

            model.allLangs = CrudFunctions.LanguageDropdown(langs);

            model.allDocs = from m in documentDef
                            select new SelectListItem() { Text = m.Name, Value = m.DocType };

            model.allPages = pages;

            return View(model);
        }

        public ActionResult Newsletter()
        {
            var documentDef = _cmsService.GetAllDocDefs().Where(x => x.Visibility.Equals(true));
            var pages = _cmsService.GetAllPages().Where(m => m.PageCategory.Equals("newsletter"));
            var langs = _cmsService.GetAllLangs();

            DocIndexViewModel model = new DocIndexViewModel();

            model.allLangs = CrudFunctions.LanguageDropdown(langs);

            model.allDocs = from m in documentDef
                            select new SelectListItem() { Text = m.Name, Value = m.DocType };

            model.allPages = pages;

            return View(model);
        }

        public ActionResult Create(String doctype, String pageCategory,  String lang = null)
        {
            DocDefinition docDef;
            DocPart doc;
            DocPartContent docContent;
            PageMap pageDetails;
            Event events;

            crFuncs.createUtils(doctype, pageCategory, lang, out docDef, out doc, out docContent, out pageDetails, out events);

            return ShowPage(pageDetails, doc, docDef, docContent);

        }

        [HttpGet]
        public ActionResult EditPage(int id, String lang = null)
        {
            //Event events;

            PageMap pageEvents;
            DocPart docpart;
            DocDefinition docDef;
            DocPartContent docPartContent;

            pageEvents = _cmsService.GetPage(id);

            crFuncs.editGetUtils(id, lang, pageEvents, out docpart, out docDef, out docPartContent);

            return ShowPage(pageEvents, docpart, docDef, docPartContent);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditPage(FormCollection collection)
        {
            int pageDataID = -1;
            int.TryParse(collection["PageMapID"], out pageDataID);
            var page = new PageMap();
            DocPart docPart = null;
            DocPartContent docPartContent = null;
            DocDefinition docDef = null;

            if (pageDataID > 0)
            {
                page = _cmsService.GetPage(pageDataID);

            }
            else
            {
                page = new PageMap();
            }

            if (page != null)
            {
                page.Title = collection["PageName"] ?? "defaultPage" + page.DocPartId;
                page.SeoName = collection["PageUrl"] ?? "defaultUrl" + page.DocPartId;
                page.ViewTemplate = collection["PageViewTemplate"] ?? "defaultViewTemplate";
                page.PageCategory = collection["PageCategory"] ?? null;

                if (collection["PageActiveCheckbox"].Contains("true"))
                {
                    page.Active = true;
                }
                else
                {
                    page.Active = false;
                }

                //crFuncs.LoadUpdateDocPart(collection, out docPart, out docPartContent, out docDef);

                var formCollectionVals = crFuncs.GetDocPartVals(collection);
                String lang = collection["lang"] ?? WebConfigurationManager.AppSettings["DefaultLanguage"];

                docPart = _cmsService.SetUpDocPart(formCollectionVals, lang);
                docPartContent = _cmsService.SetUpDocPartContent(formCollectionVals, lang);
                
                var docType = formCollectionVals.DocType;
                docDef = _cmsService.GetDocDef(docType);

                Dictionary<string, string> currentEntry = null;
                if (docPartContent.Content != null)
                {
                    currentEntry = JsonConvert.DeserializeObject<Dictionary<string, string>>(docPartContent.Content);
                }

                docPartContent.Content = JsonConvert.SerializeObject(CrudFunctions.MergeContent(collection, currentEntry));
                

                _cmsService.SavePage(page, docPart, docPartContent);
            }

            return ShowPage(page, docPart, docDef, docPartContent);
        }

        [NonAction]
        private ActionResult ShowPage(PageMap page, DocPart doc, DocDefinition docDef, DocPartContent docContent)
        {
            if (page == null || doc == null || docDef == null || docContent == null)
            {
                // return error;
            }

            var langs = _cmsService.GetAllLangs();
            var langList = CrudFunctions.LanguageDropdown(langs);

            var model = new PageViewModel(page, docDef, doc, docContent, langList);
            ViewBag.DocModel = model;

            return View("EditPage", model);
        }

        //DocPart
        public ActionResult CreateDocPart(String doctype, String lang = null, int pid = -1, string pmemeber = null)
        {
            DocDefinition docDef;
            DocPart doc;
            DocPartContent docContent;
            crFuncs.createDocPartUtils(doctype, lang, pid, pmemeber, out docDef, out doc, out docContent);

            return ShowDocPart(doc, docDef, docContent);
        }

        
        [HttpGet]
        public ActionResult EditDocPart(int id, String lang = null)
        {
            DocPart doc;
            DocDefinition docDef;
            DocPartContent docPartContent;
            crFuncs.editDocPartUtils(id, lang, out doc, out docDef, out docPartContent);

            return ShowDocPart(doc, docDef, docPartContent);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditDocPart(FormCollection collection) 
        {
            DocPart docPart = null;
            DocPartContent docPartContent = null;
            DocDefinition docDef = null;
            
            //crFuncs.LoadUpdateDocPart(collection, out docPart, out docPartContent, out docDef);

            var formCollectionVals = crFuncs.GetDocPartVals(collection);
            String lang = collection["lang"] ?? WebConfigurationManager.AppSettings["DefaultLanguage"];

            docPart = _cmsService.SetUpDocPart(formCollectionVals, lang);
            docPartContent = _cmsService.SetUpDocPartContent(formCollectionVals, lang);
            var docType = formCollectionVals.DocType;
            docDef = _cmsService.GetDocDef(docType);

            Dictionary<string, string> currentEntry = null;
            if (docPartContent.Content != null)
            {
                currentEntry = JsonConvert.DeserializeObject<Dictionary<string, string>>(docPartContent.Content);
            }

            docPartContent.Content = JsonConvert.SerializeObject(CrudFunctions.MergeContent(collection, currentEntry));
                
            if (docPart != null)
            {
                _cmsService.SaveDocPart(docPart, docPartContent);
            }

            return ShowDocPart(docPart, docDef, docPartContent);
        }

        [NonAction]
        private ActionResult ShowDocPart(DocPart doc, DocDefinition docDef, DocPartContent docContent)
        {
            if (doc == null || docDef == null || docContent == null)
            {
                // return error;
            }

            var langs = _cmsService.GetAllLangs();
            var langList = CrudFunctions.LanguageDropdown(langs);

            var model = new DocPartViewModel(docDef, doc, docContent, langList);

            ViewBag.DocModel = model;

            return View("EditDocPart", model);
        }

        //Docpart "List" items
        [HttpGet]
        public ActionResult ListItems(int parentId, string parentprop, string lang) 
        {
            if (parentprop != null && parentId > 0) 
            {
                IList<int> record = _cmsService.GetDocParts().Where(x => x.ParentProperty.Equals(parentprop) && x.ParentId.Equals(parentId)).Select(x => x.ID).ToList();

                var recordList = new List<DocPartContent>();
                
                foreach (var itemId in record)
                {
                    var item = _cmsService.GetDocPartContents().OrderBy(x => x.Language != lang).FirstOrDefault(x => x.DocPartId.Equals(itemId));
                    recordList.Add(item);
                }

                return Json(recordList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { id = 0, data = "Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
