using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using MB9.Core.Web.Services;
using MB9.Core.Service;
using MB9.CMS.Core.Services;
using MB9.Core.Entity;
using MB9.CMS.Core.CMS.Entity;
using MB9.CMS.Core.EMS.Entity;

namespace MB9.CMS.Web
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IAppSession, AppSession>();

            //*** User Module
            container.RegisterType<IUserService, UserService>();
            container.RegisterType(typeof(UserRepository<>), typeof(UserRepository<>));

            //***** CMS Module
            container.RegisterType<ICmsService, CmsService>();
            container.RegisterType(typeof(CmsRepository<>), typeof(CmsRepository<>));

            //***** Event Module
            container.RegisterType<IEmsService, EmsService>();
            container.RegisterType(typeof(EmsRepository<>), typeof(EmsRepository<>));

            //***** CMS Module
            container.RegisterType<IMenuService, MenuService>();
            container.RegisterType(typeof(CmsRepository<>), typeof(CmsRepository<>));


           
            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {

        }



    }
}