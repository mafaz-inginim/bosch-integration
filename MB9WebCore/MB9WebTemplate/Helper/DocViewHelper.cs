﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MB9.CMS.Web.Helper
{
    public static class DocViewHelper
    {
        public const string MEMBER_SAVE_PREFIX = "saveid.";

        public static string GetMemberSaveID(string name)
        {
            return String.Format("{0}{1}", MEMBER_SAVE_PREFIX, name);
        }

        public static string GetMemberNameFromSaveID(string saveName)
        {
            if (saveName.StartsWith(MEMBER_SAVE_PREFIX))
            {
                return saveName.Substring(MEMBER_SAVE_PREFIX.Length);
            }
            return null;
        }

    }
}