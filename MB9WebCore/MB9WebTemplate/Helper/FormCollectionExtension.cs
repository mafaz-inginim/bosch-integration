﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Helper
{
    public static class FormCollectionExtension
    {
        public static int GetIntVal(this FormCollection collection, string key, int defaultVal = -1)
        {
            int outVal = defaultVal;
            int.TryParse(key, out outVal);
            return outVal;
        }

        public static bool GetBoolVal(this FormCollection collection, string key, bool defaultVal = false)
        {
            bool outVal = defaultVal;
            bool.TryParse(key, out outVal);
            return outVal;
        }

        public static string GetNonIntVal(this FormCollection collection, string key, string defaultVal = "")
        {

            if (key != null)
            {
                var item = key;
                return item;
            }
            else { return defaultVal; }
            
        }
    }
}