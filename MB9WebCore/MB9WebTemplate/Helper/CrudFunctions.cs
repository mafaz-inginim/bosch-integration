﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using MB9.Core.Web.Services;
using MB9.CMS.Core.Services;
using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.EMS.Model;

namespace MB9.CMS.Web.Helper
{
    public class CrudFunctions
    {
        private readonly ICmsService _cmsService;
        private readonly IEmsService _emsService;

        public CrudFunctions(ICmsService cmsService, IEmsService emsService)
        {
            this._cmsService = cmsService;
            this._emsService = emsService;
        }

        public static IEnumerable<SelectListItem> LanguageDropdown(IEnumerable<Language> langs)
        {
            var LangList = from n in langs
                           select new SelectListItem() { Text = n.languageDisplayName, Value = n.languageCode };
            return LangList;
        }

        public void createUtils(String doctype, String pageCateg, String lang, out DocDefinition docDef, out DocPart doc, out DocPartContent docContent, out PageMap pageDetails, out Event events)
        {
            docDef = _cmsService.GetDocDef(doctype);

            doc = new DocPart()
            {
                ParentId = -1,
                ParentProperty = String.Empty
            };

            docContent = new DocPartContent()
            {
                Language = lang ?? WebConfigurationManager.AppSettings["DefaultLanguage"]
            };

            pageDetails = new PageMap()
            {
               DocPartId = doc.ID,
               PageCategory = pageCateg
            };
            
            events = new Event()
            {
               EventType = doctype
            };

        }

        public void editGetUtils(int id, String lang, IAbstractPage page, out DocPart docpart, out DocDefinition docDef, out DocPartContent docPartContent)
        {
            var lng = lang ?? WebConfigurationManager.AppSettings["DefaultLanguage"];

            docpart = page != null ? _cmsService.GetDocPart(page.DocPartId) : null;
            docDef = page != null ? _cmsService.GetDocDef(docpart.DocType) : null;

            docPartContent = _cmsService.GetDocPartContent(docpart, lng) ??
            (docpart != null
            ? new DocPartContent() { DocPartId = docpart.ID, Language = lng }
            : null);
        }

        public void createDocPartUtils(String doctype, String lang, int pid, string pmemeber, out DocDefinition docDef, out DocPart doc, out DocPartContent docContent)
        {
            docDef = _cmsService.GetDocDef(doctype);

            doc = new DocPart()
            {
                ParentId = pid,
                ParentProperty = pmemeber
            };

            docContent = new DocPartContent()
            {
                Language = lang ?? WebConfigurationManager.AppSettings["DefaultLanguage"]
            };

            var pageDetails = new PageMap()
            {
                DocPartId = doc.ID
            };
        }

        public void editDocPartUtils(int id, String lang, out DocPart doc, out DocDefinition docDef, out DocPartContent docPartContent)
        {
            var lng = lang ?? WebConfigurationManager.AppSettings["DefaultLanguage"];

            doc = _cmsService.GetDocPart(id);

            docDef = doc != null ? _cmsService.GetDocDef(doc.DocType) : null;

            docPartContent = _cmsService.GetDocPartContent(id, lng)
            ?? (docDef != null
            ? new DocPartContent() { DocPartId = id, Language = lng }
            : null);
        }

     
        public DocPart GetDocPartVals(FormCollection collection)
        {
            return new DocPart()
            {
                ID = collection.GetIntVal(collection[ViewStrings.DocPartID]),
                ParentId = collection.GetIntVal(collection[ViewStrings.parentId]),
                DocType = collection.GetNonIntVal(collection[ViewStrings.docType]),
                ParentProperty = collection.GetNonIntVal(collection[ViewStrings.parentProp]),
                Published = collection.GetBoolVal(collection[ViewStrings.published])
            };

        }

        //public void LoadUpdateDocPart(FormCollection collection, out DocPart docPart, out DocPartContent docPartContent, out DocDefinition docDef)
        //{
        //    var formCollectionVals = GetDocPartVals(collection);

        //    var docType = formCollectionVals.DocType;

        //    docDef = _cmsService.GetDocDef(docType);
        //    int docPartID = formCollectionVals.ID;
        //    docPart = null;
        //    docPartContent = null;
        //    String lang = collection["lang"] ?? WebConfigurationManager.AppSettings["DefaultLanguage"];
            
        //    if (docDef != null && lang != null)
        //    {
        //        if (docPartID > 0)
        //        {
        //            docPart = _cmsService.GetDocPart(docPartID);
        //            docPartContent = _cmsService.GetDocPartContent(docPartID, lang);
        //        }
        //        else
        //        {
        //            docPart = new DocPart();
        //            int parentID = formCollectionVals.ParentId;
        //            docPart.ParentId = parentID;

        //            docPart.ParentProperty = formCollectionVals.ParentProperty;

        //            docPart.DocType = docDef.DocType;
        //        }

        //        if (docPart != null)
        //        {
        //            bool published = formCollectionVals.Published;
        //            docPart.Published = published;

        //            docPartContent = docPartContent ?? new DocPartContent() { Language = lang, DocPartId = docPartID };

        //            Dictionary<string, string> currentEntry = null;

        //            if (docPartContent.Content != null)
        //            { 
        //                currentEntry = JsonConvert.DeserializeObject<Dictionary<string, string>>(docPartContent.Content); 
        //            }

        //            docPartContent.Content = JsonConvert.SerializeObject(MergeContent(collection, currentEntry));
        //        }

        //    }
        //}

        public static Dictionary<String, string> MergeContent(FormCollection collection, Dictionary<String, String> currentContent = null)
        {
            var content = currentContent ?? new Dictionary<String, string>();
            foreach (var key in collection.AllKeys)
            {
                var memberid = DocViewHelper.GetMemberNameFromSaveID(key);
                if (memberid != null)
                {
                    content[memberid] = collection[key];
                }
            }

            return content;
        }
    }
}