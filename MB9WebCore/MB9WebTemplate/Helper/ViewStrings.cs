﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MB9.CMS.Web.Helper
{
    public class ViewStrings
    {
        public const String DocPartID = "docPartID_Doc";
        public const String lang = "lang";
        public const String docType = "docType";
        public const String parentId = "parent.id";
        public const String parentProp = "parent.prop";
        public const String published = "published";
    }
}