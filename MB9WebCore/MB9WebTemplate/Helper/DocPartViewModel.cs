﻿using MB9.CMS.Core.CMS.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Helper
{
    public class DocPartViewModel
    {
        public DocPartViewModel(DocDefinition def, DocPart doc, DocPartContent docContent, IEnumerable<SelectListItem> Langlist)
        {
            Def = def;

            DocPart = doc;

            DocContent = docContent;

            Language = docContent.Language;

            Definition = (def != null && def.Definition != null) ?
                    JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(def.Definition)
                    : new List<Dictionary<string, string>>();

            Content = (DocContent != null && DocContent.Content != null) ?
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(DocContent.Content)
                    : new Dictionary<string, string>();

            allLangs = Langlist;

        }

        public DocDefinition Def { get; private set; }

        public DocPart DocPart { get; private set; }

        public DocPartContent DocContent { get; private set; }

        public String Language { get; private set; }

        public IEnumerable<SelectListItem> allLangs { get; set; }

        public IEnumerable<SelectListItem> settings { get; set; }

        public IList<Dictionary<string, string>> Definition { get; private set; }

        public Dictionary<string, string> Content { get; private set;}

        public Dictionary<string, string> ListItems { get; private set; }

        public string GetContentId(string memeberid)
        {
            String val = String.Empty;

            Content.TryGetValue(memeberid, out val);

            return val;
        }
    }
}