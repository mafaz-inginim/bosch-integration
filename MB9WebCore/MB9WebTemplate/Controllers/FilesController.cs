﻿using ElFinder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MB9.CMS.Web.Helper
{
    public class FilesController : Controller
    {
        private Connector _connector;

        public Connector Connector
        {
            get
            {
                if (_connector == null)
                {
                    FileSystemDriver driver = new FileSystemDriver();
                    DirectoryInfo thumbsStorage = new DirectoryInfo(Server.MapPath("~/App_Data/images/testThumbs"));
                    //driver.AddRoot(new Root(new DirectoryInfo(@"C:\Program Files"))
                    //{
                    //    IsLocked = true,
                    //    IsReadOnly = true,
                    //    IsShowOnly = true,
                    //    ThumbnailsStorage = thumbsStorage,
                    //    ThumbnailsUrl = VirtualPathUtility.ToAbsolute("~/Thumbnails/")
                    //});
                    driver.AddRoot(new Root(new DirectoryInfo(Server.MapPath("~/Content/resources/images")), VirtualPathUtility.ToAbsolute("~/Content/resources/images/"))
                    {
                        Alias = "images",
                        StartPath = new DirectoryInfo(Server.MapPath("~/Content/resources/images")),
                        ThumbnailsStorage = thumbsStorage,
                        MaxUploadSizeInMb = 2.2,
                        ThumbnailsUrl = VirtualPathUtility.ToAbsolute("~/Thumbnails/")
                    });
                    driver.AddRoot(new Root(new DirectoryInfo(Server.MapPath("~/Content/resources/docs")), VirtualPathUtility.ToAbsolute("~/Content/resources/docs/"))
                    {
                        Alias = "docs",
                        StartPath = new DirectoryInfo(Server.MapPath("~/Content/resources/docs")),
                        ThumbnailsStorage = thumbsStorage,
                        MaxUploadSizeInMb = 2.2,
                        ThumbnailsUrl = VirtualPathUtility.ToAbsolute("~/Thumbnails/")
                    });
                    _connector = new Connector(driver);
                }
                return _connector;
            }
        }
        public ActionResult FilePicker()
        {
            return Connector.Process(this.HttpContext.Request);
        }

        public ActionResult SelectFile(string target)
        {
            return Json(Connector.GetFileByHash(target).FullName);
        }

        public ActionResult FileSelect(string target)
        {
            return Json(Connector.GetFileByHash(target).Name);
        }
        public ActionResult Thumbs(string tmb)
        {
            return Connector.GetThumbnail(Request, Response, tmb);
        }

        public ActionResult ShowPicker()
        {
            return View();
        }

    }
}
