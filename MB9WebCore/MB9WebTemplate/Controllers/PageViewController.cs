﻿using MB9.CMS.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using MB9.Core.Web.Services;
using MB9.CMS.Core.Services;
using MB9.CMS.Core.CMS.Model;
using MB9.CMS.Core.EMS.Model;
using MB9.CMS.Core.CMS.ViewModel;
using System.Web.Caching;

namespace MB9.CMS.Web.Controllers
{
    public class PageViewController : Controller
    {
        private readonly ICmsService _cmsService;
        private readonly IEmsService _emsService;
        private readonly IMenuService _menuService;

        public PageViewController(ICmsService cmsService, IEmsService emsService, IMenuService menuService)
        {
            this._cmsService = cmsService;
            this._emsService = emsService;
            this._menuService = menuService;
        }

        //
        // GET: /PageView/

        public ActionResult Index(string url, string lang = null)
        {
            lang = lang ?? WebConfigurationManager.AppSettings["DefaultLanguage"];

            var page = _cmsService.GetAllPages().FirstOrDefault(x => x.SeoName == url);

            var docpart = _cmsService.GetDocParts().FirstOrDefault(x => x.ID == page.DocPartId);
            var doccontent = _cmsService.GetDocPartContents().FirstOrDefault(x => x.DocPartId.Equals(docpart.ID) && x.Language.Equals(lang));

            List<int> listContent = _cmsService.GetDocParts().Where(x => x.ParentId.Equals(page.DocPartId)).Select(x => x.ID).ToList();
            var recordList = new List<DocPartContent>();

            if (listContent.Count > 0)
            {
                foreach (var itemId in listContent)
                {
                    var item = _cmsService.GetDocPartContents().OrderBy(x => x.Language != lang).FirstOrDefault(x => x.DocPartId.Equals(itemId));
                    recordList.Add(item);
                }

                ViewBag.ListData = recordList;
            }

            Event events = null;

            var model = new TemlatePreviewViewModel(docpart, doccontent, page, events);

            return View(model);
        }

        public ActionResult EventPreview(string url, string lang = null) 
        {
            lang = lang ?? WebConfigurationManager.AppSettings["DefaultLanguage"];

            var events = _emsService.GetAllEvents().FirstOrDefault(x => x.SeoName == url);
            var docpart = _cmsService.GetDocParts().FirstOrDefault(x => x.ID == events.DocPartId);
            var doccontent = _cmsService.GetDocPartContents().FirstOrDefault(x => x.DocPartId.Equals(docpart.ID) && x.Language.Equals(lang));

            PageMap page = null;

            var model = new TemlatePreviewViewModel(docpart, doccontent, page, events);

            return View(model);
        }

        public ActionResult DisplayMenuBar(string language = "ENG")
        {
            MenuView menu_tree = HttpRuntime.Cache.Get(language + "_menu") as MenuView;
            if (menu_tree == null)
            {
                menu_tree = _menuService.GetMenuAsTree(language,"display");

                HttpRuntime.Cache.Insert(language + "_menu", menu_tree, null, DateTime.UtcNow.AddHours(1), Cache.NoSlidingExpiration);

            }
            return PartialView("_displayMenu",menu_tree);
        }



    }
}
