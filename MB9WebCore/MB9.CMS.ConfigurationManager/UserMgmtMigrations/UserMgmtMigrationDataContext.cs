﻿using MB9.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MB9.CMS.ConfigurationManager.UserMgmtMigrations
{
    public class UserMgmtMigrationDataContext : UserManagementContext
    {
        public UserMgmtMigrationDataContext()
            : this("MigrationConnection")
        {

        }

        public UserMgmtMigrationDataContext(String nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }
    }
}