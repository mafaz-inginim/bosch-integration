namespace MB9.CMS.ConfigurationManager.UserMgmtMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserRoleId = c.Int(nullable: false),
                        DisplayName = c.String(),
                        UserName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        PasswordSalt = c.String(),
                        Active = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserRoles", t => t.UserRoleId, cascadeDelete: true)
                .Index(t => t.UserRoleId);
            
            CreateTable(
                "dbo.UserRolePermissions",
                c => new
                    {
                        UserRole_Id = c.Int(nullable: false),
                        Permission_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserRole_Id, t.Permission_Id })
                .ForeignKey("dbo.UserRoles", t => t.UserRole_Id, cascadeDelete: true)
                .ForeignKey("dbo.Permissions", t => t.Permission_Id, cascadeDelete: true)
                .Index(t => t.UserRole_Id)
                .Index(t => t.Permission_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "UserRoleId", "dbo.UserRoles");
            DropForeignKey("dbo.UserRolePermissions", "Permission_Id", "dbo.Permissions");
            DropForeignKey("dbo.UserRolePermissions", "UserRole_Id", "dbo.UserRoles");
            DropIndex("dbo.UserRolePermissions", new[] { "Permission_Id" });
            DropIndex("dbo.UserRolePermissions", new[] { "UserRole_Id" });
            DropIndex("dbo.Users", new[] { "UserRoleId" });
            DropTable("dbo.UserRolePermissions");
            DropTable("dbo.Users");
            DropTable("dbo.UserRoles");
            DropTable("dbo.Permissions");
        }
    }
}
