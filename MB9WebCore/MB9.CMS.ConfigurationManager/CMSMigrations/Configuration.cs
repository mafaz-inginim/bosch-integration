namespace MB9.CMS.ConfigurationManager.CMSMigrations
{
    using MB9.CMS.Core.CMS.Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MB9.CMS.ConfigurationManager.CMSMigrations.CMSMigrationDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"CMSMigrations";
        }

        protected override void Seed(MB9.CMS.ConfigurationManager.CMSMigrations.CMSMigrationDataContext context)
        {
            if (context.Settings == null || context.Settings.ToList().Count() <= 0)
            {
                //Default Languages
                Language defautLanguage_1 = new Language();
                defautLanguage_1.languageCode = "EN";
                defautLanguage_1.languageDisplayName = "ENG";
                defautLanguage_1.languageName = "English US/UK";
                context.Languages.Add(defautLanguage_1);
                context.SaveChanges();

                Language defautLanguage_2 = new Language();
                defautLanguage_2.languageCode = "ZH";
                defautLanguage_2.languageDisplayName = "CHN";
                defautLanguage_2.languageName = "Chinese";
                context.Languages.Add(defautLanguage_2);
                context.SaveChanges();

                //Default Settings
                Settings defautsettings_1 = new Settings();
                defautsettings_1.Name = "event.registration.status";
                defautsettings_1.Value = "CNF, REJ, Pending";
                context.Settings.Add(defautsettings_1);
                context.SaveChanges();

                Settings defautsettings_2 = new Settings();
                defautsettings_2.Name = "gender.menu";
                defautsettings_2.Value = "Male, Female";
                context.Settings.Add(defautsettings_2);
                context.SaveChanges();

                //Default Page Template
                DocDefinition defautdef = new DocDefinition();
                defautdef.DocType = "dboardpage";
                defautdef.Name = "Board Memebr Page";
                defautdef.Definition = "[ { \"id\" : \"banner\", \"name\" : \"Banner Image\", \"type\" : \"image\", \"desc\" : \"page banner image size 1200 x 200\" }, { \"id\" : \"title\", \"name\" : \"Page Title\", \"type\" : \"text\", \"desc\" : \"Tile of the page\" }, { \"id\" : \"description\", \"name\" : \"Body\", \"type\" : \"htmltextarea\" } ]";
                defautdef.Visibility = true;
                context.DocDefinitions.Add(defautdef);
                context.SaveChanges();

                //Default Event Template
                DocDefinition defauteventdef = new DocDefinition();
                defauteventdef.DocType = "eventTemplate";
                defauteventdef.Name = "Annual Meet";
                defauteventdef.Definition = "[ { \"id\" : \"banner\", \"name\" : \"EventBanner\", \"type\" : \"image\", \"desc\" : \"page banner image size 1200 x 200\" }, { \"id\" : \"topic\", \"name\" : \"Topic\", \"type\" : \"text\", \"desc\" : \"event topic\" }, { \"id\" : \"venue\", \"name\" : \"EventVenue\", \"type\" : \"text\", \"desc\" : \"Place of the scheduled event\" }, { \"id\" : \"shortdesc\", \"name\" : \"ShortDesc\", \"type\" : \"htmltextarea\", \"desc\" : \"ShortDesc of the scheduled event\" }, { \"id\" : \"host\", \"name\" : \"HostName\", \"type\" : \"text\", \"desc\" : \"event Host Name\" }, { \"id\" : \"intro\", \"name\" : \"Introduction\", \"type\" : \"textarea\", \"desc\" : \"Introduction of the scheduled event\" }, { \"id\" : \"progdesc\", \"name\" : \"ProgDetails\", \"type\" : \"htmltextarea\", \"desc\" : \"Program Details of the scheduled event\" }, { \"id\" : \"attendees\", \"name\" : \"ExpectedAttendees\", \"type\" : \"text\", \"desc\" : \"Expected Attendees of the scheduled event\" }, { \"id\" : \"facilitators\", \"name\" : \"Facilitators\", \"type\" : \"text\", \"desc\" : \"Facilitators of the scheduled event\" }, { \"id\" : \"brochure\", \"name\" : \"Brochure\", \"type\" : \"doc\", \"desc\" : \"Brochure of the scheduled event\" } ]";
                defauteventdef.Visibility = false;
                context.DocDefinitions.Add(defauteventdef);
                context.SaveChanges();

                //Default Blog Template
                DocDefinition defautblogdef = new DocDefinition();
                defautblogdef.DocType = "blog";
                defautblogdef.Name = "simple blog";
                defautblogdef.Definition = "[ { \"id\" : \"banner\", \"name\" : \"Banner Image\", \"type\" : \"image\", \"desc\" : \"Blog banner image size 1200 x 200\" }, { \"id\" : \"title\", \"name\" : \"Blog Title\", \"type\" : \"text\", \"desc\" : \"Tile of the Blog\" }, { \"id\" : \"description\", \"name\" : \"Body\", \"type\" : \"htmltextarea\" } ]";
                defautblogdef.Visibility = false;
                context.DocDefinitions.Add(defautblogdef);
                context.SaveChanges();

                //Default Newsletter Template
                DocDefinition defautnewsdef = new DocDefinition();
                defautnewsdef.DocType = "newsletter";
                defautnewsdef.Name = "simple Newsletter";
                defautnewsdef.Definition = "[ { \"id\" : \"title\", \"name\" : \"Newsletter Title\", \"type\" : \"text\", \"desc\" : \"Tile of the Newsletter\" }, { \"id\" : \"banner\", \"name\" : \"Newsletter Image\", \"type\" : \"image\", \"desc\" : \"Newsletter banner image size 1200 x 200\" }, { \"id\" : \"intoduction\", \"name\" : \"Newsletter Intro\", \"type\" : \"text\", \"desc\" : \"Introduction of the Newsletter\" }, { \"id\" : \"description\", \"name\" : \"Body\", \"type\" : \"htmltextarea\", }, { \"id\" : \"note\", \"name\" : \"Newsletter footer\", \"type\" : \"text\", \"desc\" : \"footer of the Newsletter\" } ]";
                defautnewsdef.Visibility = false;
                context.DocDefinitions.Add(defautnewsdef);
                context.SaveChanges();

            }
        }
    }
}
