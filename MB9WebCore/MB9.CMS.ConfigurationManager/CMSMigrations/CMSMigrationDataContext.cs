﻿using MB9.CMS.Core.CMS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MB9.CMS.ConfigurationManager.CMSMigrations
{
    public class CMSMigrationDataContext : CmsEntitiesDBContext
    {
        public CMSMigrationDataContext()
            : this("MigrationConnection")
        {

        }

        public CMSMigrationDataContext(String nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }
    }
}