namespace MB9.CMS.ConfigurationManager.CMSMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DocDefinition",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DocType = c.String(),
                        Name = c.String(),
                        Definition = c.String(unicode: false, storeType: "text"),
                        Visibility = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DocPartContent",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DocPartId = c.Int(nullable: false),
                        Content = c.String(storeType: "ntext"),
                        Language = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DocPart",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DocType = c.String(),
                        ParentId = c.Int(nullable: false),
                        ParentProperty = c.String(),
                        Published = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Language",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        languageCode = c.String(),
                        languageDisplayName = c.String(),
                        languageName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PageMap",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SeoName = c.String(),
                        Title = c.String(),
                        DocPartId = c.Int(nullable: false),
                        PageCategory = c.String(),
                        ViewTemplate = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ViewTemplate",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DisplayName = c.String(),
                        DocType = c.String(),
                        Key = c.String(),
                        Value = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ViewTemplate");
            DropTable("dbo.Settings");
            DropTable("dbo.PageMap");
            DropTable("dbo.Language");
            DropTable("dbo.DocPart");
            DropTable("dbo.DocPartContent");
            DropTable("dbo.DocDefinition");
        }
    }
}
