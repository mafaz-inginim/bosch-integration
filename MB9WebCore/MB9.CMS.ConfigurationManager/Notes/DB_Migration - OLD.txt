
======================================================================================================
  NOTE: Set the connection string and correct db name in webconfig:MB9.POB.ConfigurationManager
======================================================================================================

NOTE: FIRST TIME SETUP NEED TO BE DONE USING INSTALL;

STEPS TO UPDATE DATABASE MIGRATIONS
====================================
1. Open Package Manager Console
	(Tools > Library Package Manager > Package Manager Console)
	
2. If any new modifcation added to the migrations then run:
	Add-Migration -name <CHANGE_LABEL_HERE> -ProjectName MB9.CMS.ConfigurationManager -StartUpProjectName MB9.CMS.ConfigurationManager  -verbose

3: To update database:
	Update-Database -ProjectName MB9.CMS.ConfigurationManager -StartUpProjectName MB9.CMS.ConfigurationManager -Verbose
	


Initlaize A Fresh Database with exiting migration
=================================================
1. Open Package Manager Console
	(Tools > Library Package Manager > Package Manager Console)

2. Modify the line in  MB9.CMS.ConfigurationManager.Migrations.Configguration  project: MB9.CMS.ConfigurationManager
   set the variable AutomaticMigrationsEnabled = true in constructure

3. Modify Web.Config in project MB9.CMS.ConfigurationManager  to correct connection string

4. Run Migrations
   Update-Database -ProjectName MB9.CMS.ConfigurationManager -StartUpProjectName MB9.CMS.ConfigurationManager -Verbose




Initial Setup to add migration to a project (not required to run each time)
============================================
1. Install-Package:
	Install-Package EntityFramework

2. Enable-Migrations:
	Enable-Migrations -ContextProjectName MB9.CMS.Core -StartUpProjectName MB9.CMS.ConfigurationManager -ContextTypeName MB9.CMS.Core.EMS.Entity.EmsEntitiesDBContext -ProjectName MB9.CMS.ConfigurationManager -Verbose

3. Add-Migrations:
	Add-Migration -name InitialMigration -ProjectName MB9.CMS.ConfigurationManager -StartUpProjectName MB9.CMS.Web-verbose

3. Update:
	Update-Database -ProjectName MB9.CMS.ConfigurationManager -StartUpProjectName MB9.CMS.ConfigurationManager -Verbose


