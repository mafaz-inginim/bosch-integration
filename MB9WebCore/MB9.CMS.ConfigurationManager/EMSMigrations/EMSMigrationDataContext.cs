﻿using MB9.CMS.Core.EMS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MB9.CMS.ConfigurationManager.EMSMigrations
{
    public class EMSMigrationDataContext : EmsEntitiesDBContext
    {
        public EMSMigrationDataContext()
            : this("MigrationConnection")
        {

        }

        public EMSMigrationDataContext(String nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }
    }
}