namespace MB9.CMS.ConfigurationManager.EMSMigrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MB9.CMS.ConfigurationManager.EMSMigrations.EMSMigrationDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"EMSMigrations";
        }

        protected override void Seed(MB9.CMS.ConfigurationManager.EMSMigrations.EMSMigrationDataContext context)
        {
            //if (context.Users == null || context.Users.ToList().Count() <= 0)
            //{
            //    UserRole defautrole = new UserRole();
            //    defautrole.Name = "Admin";
            //    defautrole.Active = true;
            //    context.UserRoles.Add(defautrole);
            //    context.SaveChanges();
            //    User defaultuser = new User();
            //    defaultuser.DisplayName = "cmsadmin";
            //    defaultuser.UserName = "cmsadmin";
            //    defaultuser.Password = PasswordUtil.SaltedHash("admin");
            //    defaultuser.Email = "admin@cms.com";
            //    defaultuser.Active = true;
            //    defaultuser.UserRoleId = defautrole.Id;
            //    context.Users.Add(defaultuser);
            //    context.SaveChanges();

            //}
        }
    }
}
