namespace MB9.CMS.ConfigurationManager.EMSMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EventAttendee",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EventID = c.Int(nullable: false),
                        EventRegisterID = c.Int(nullable: false),
                        EmailID = c.String(),
                        AttendedEvent = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EventRegister",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EventID = c.Int(nullable: false),
                        EmailID = c.String(),
                        Name = c.String(),
                        PhoneNum = c.String(),
                        RegistrationStatus = c.String(),
                        AdditionalContent = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EventType = c.String(),
                        EventStartDate = c.DateTime(nullable: false),
                        EventEndDate = c.DateTime(nullable: false),
                        RegStartDate = c.DateTime(nullable: false),
                        RegEndDate = c.DateTime(nullable: false),
                        MaxAttendees = c.Int(nullable: false),
                        SeoName = c.String(),
                        Title = c.String(),
                        DocPartId = c.Int(nullable: false),
                        PageCategory = c.String(),
                        ViewTemplate = c.String(),
                        Active = c.Boolean(nullable: false),
                        AutoInclusion = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Event");
            DropTable("dbo.EventRegister");
            DropTable("dbo.EventAttendee");
        }
    }
}
