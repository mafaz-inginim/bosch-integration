namespace MB9.CMS.ConfigurationManager.EMSMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventAttendeeMods : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventAttendee", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventAttendee", "Name");
        }
    }
}
