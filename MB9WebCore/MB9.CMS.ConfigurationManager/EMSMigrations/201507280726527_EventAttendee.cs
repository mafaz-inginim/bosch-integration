namespace MB9.CMS.ConfigurationManager.EMSMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventAttendee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventAttendee", "AlumniMember", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventAttendee", "AlumniMember");
        }
    }
}
