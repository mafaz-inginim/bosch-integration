﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MB9WebCore.Startup))]
namespace MB9WebCore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
